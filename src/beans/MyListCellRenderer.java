
package beans;

import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author maria
 */
public class MyListCellRenderer implements ListCellRenderer {

    JLabelButForJList jlab=new JLabelButForJList();
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
       
        JLabelButForJList action = (JLabelButForJList) value;
        if (isSelected) {
            jlab.setImage(2);
        } else {
            jlab.setImage(0);
        }
        jlab.setText(action.getText());
        jlab.setSize(new Dimension(180,25));
        action.setSize(new Dimension(180,25));
////        action.setSize(new java.awt.Dimension(100, 35));
////            setEnabled(list.isEnabled());
////            setFont(list.getFont());
//
//////            setOpaque(true);
//        jlab.setOpaque(isSelected);
        return jlab;
    }

}
