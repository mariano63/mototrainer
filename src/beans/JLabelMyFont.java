package beans;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;
import javax.swing.JLabel;

/**
 *
 * @author maria
 */
public class JLabelMyFont extends JLabel {

    protected Font myFont;

    /**
     * Creates new form JLabelMyFont
     */
    public JLabelMyFont() {
        initComponents();
        setMyFont();
    }

    public Font getMyFont() {
        return myFont;
    }

    public final void setMyFont() {
        try {
            InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream("res/steelfish_rg.otf");
            myFont = Font.createFont(Font.TRUETYPE_FONT, is).deriveFont(24f);
            setFont(myFont);
        } catch (FontFormatException | IOException ex) {
            System.err.println("Not loaded");
        }
    }

    public final void setFontSize18() {
        Font currentFont = getFont();
        Font newFont = currentFont.deriveFont(18f);
        setFont(newFont);
    }

    public final void setFontSize24() {
        Font currentFont = getFont();
        Font newFont = currentFont.deriveFont(24f);
        setFont(newFont);
    }

    public final void setFontSize32() {
        Font currentFont = getFont();
        Font newFont = currentFont.deriveFont(32f);
        setFont(newFont); 
    }
    public final void newSize(int width, int height){
        setSize(width, height);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
