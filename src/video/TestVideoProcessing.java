package video;

import processing.core.*;

import ddf.minim.*;
import static java.awt.Frame.MAXIMIZED_BOTH;
import static java.awt.Frame.ICONIFIED;
import processing.serial.*;
import processing.video.*;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.atomic.AtomicBoolean;
import mototrainer.JDialogSaveMasterTrack;
import mototrainer.Mototrainer;
import mototrainer.MyProperties;
import mototrainer.WbNew;
import mototrainer.interPol;
import processing.data.StringList;

import grafica.*;
import java.io.File;
import static processing.core.PConstants.LEFT;

public class TestVideoProcessing extends PApplet {

    //<>// //<>//
// import controlP5.*;
// static final String PATH = "D:/Progetti/Starbyte/SimulatoreMoto/";
    static final String MY_PATH = MyProperties.CONF_PATH_CIRCUITS;
    static public TestVideoProcessing iTestVideoProcessing = null;

    static final boolean DISBLE_ESC = false;

// Note: this program runs only in the Processing IDE, not in the browser
    static String MOVIE_PATH = WbNew.circuitoSelezionato;
    static final String MOVIE_PATH_ERR_FRENO_ANT = "../errorVideo/frontbrakeError.mp4";
    static final String MOVIE_PATH_ERR_FRENO_POS = "../errorVideo/rearbrakeError.mp4";
    static final String MOVIE_PATH_ERR_ACCEL = "../errorVideo/throttleError.mp4";
    static final String MOVIE_PATH_ERR_ANGOLO = "../errorVideo/leanError.mp4";
    static final String MOVIE_PATH_ERR_MARCE = "../errorVideo/gearError.mp4";
    static String TRACCIA_MADRE = WbNew.TracciaMadreSelezionata;
    static final String LOGO_IMG = "../Grafica/logo.png";
    static final String PATH_BUTTON = "../Grafica/bottoni/";
    static final String SEPARATORE = "|";

    public static List<String> ListaPunti = new ArrayList<>();

    float xxxx = 0;

    static final int COUNT_DOWN = 5;

    static final int INTERVALLO_MS = 10;    // 10 ms x campionamento traccia madre

    Movie theMov;
    Movie theMovMarciaError;
    Movie theMovFrenoAntError;
    Movie theMovFrenoPosError;
    Movie theMovAccelError;
    Movie theMovAngoloError;

    int x;
    int imgx, imgy;
    int incx, incy;

    int newMillis;
    int primoFrame;

    int nLoop;
    int currIndex;

    float duration;

    cMarce gcMarce;
    cAnalog gcAnalog;

    int newTimer;

    int countDown;

    interPol cAnalog0;
    interPol cAnalog1;
    interPol cAnalog2;

    cHandleData gcHandleData;

    leggiFile tracciaMadre;

    cLogo gcLogo;

    int Status;
    int SetupDone;

    cBottone exitButton;
    cBottone startButton;
    VersionClass Version;
    PrecountClass gcPrecount;
    public ErrorClass cErrorClass;
    SaveFileClass cSaveFileClass;
    dialogBox saveYesNo;
    done doneMsg;
    cBottone exitButtonPlotter;

    DisegnaGraficoClass xDisegnaGraficoClass;
    lap reportLap;
    int visualGraph = 0;
    
    InfoClass cInfoClass;

    // int timerLogo;
    int contatoreGiri;
    int numeroGiri;
    int STopTimer=0;
    long cronTimer;
    
    static final int SETUP = 2;
    static final int PRECOUNT = 3;
    static final int PLAY = 4;
    static final int READY_TO_START = 1;
    static final int MASTER_PAGE = 5;
    static final int INIT_PAGE = 6;
    static final int PLAY_ERROR = 7;
    static final int DO_NOTHING = 8;
    static final int RETURN_FROM_WORKBENCH = 9;
    static final int INIT_GRAPH = 10;
    static final int DRAW_GRAPH = 11;
    static final int WAIT_FOR_YES_NO = 12;
    static final int SAVE_DONE = 13;
    static final int CHKECK_SERIAL = 14;

//public void init(){
//  frame.removeNotify();
//  frame.setUndecorated(true);
//  frame.addNotify();
//  //super.init();
//}
    int EndErroMovie;
    MyObs myObs = new MyObs();
    
    int cTest;

    public TestVideoProcessing() {
    }

    public void subSetup() {
        if (Status != SetupDone) {

            SetupDone = Status;
            cErrorClass = new ErrorClass();
            cErrorClass.setSkillLEvel(1);
            Version = new VersionClass();
            gcMarce = new cMarce(10, 200, 450, displayHeight, 6, MyProperties.CONF_PATH + "Grafica/marce", 5, 5);
            gcAnalog = new cAnalog(displayWidth - 500, displayHeight - 300, 20, 20, MyProperties.CONF_PATH + "Grafica/analog");
            gcHandleData = new cHandleData();
            gcPrecount = new PrecountClass();

            openSerial(0);
            // istanziazione e lettura file traccia madre

            // fill coordinates
            x = 0;
            incx = 1;
            incy = 1;
            imgx = 10;
            imgy = 600;

            primoFrame = 0;

            nLoop = 4;

            /* only use 1 of the following options */
            // theMov.play();  //plays the movie once
            //theMov.loop();  //plays the movie over and over
            gcMarce.setMarcia(3);
            gcMarce.initMarciaRiferimanto(3);
            newTimer = millis() + 2;
            countDown = COUNT_DOWN + 1;

            exitButton = new cBottone(MY_PATH + PATH_BUTTON, "buttonExit", 50, 100, 50);
            startButton = new cBottone(MY_PATH + PATH_BUTTON, "buttonStart", 50, 100, 50);
            loadAudioError();
            cSaveFileClass = new SaveFileClass();
            saveYesNo = new dialogBox();
            doneMsg = new done();
            

            /////////////////////
            iTestVideoProcessing = this;
            WbNew.istTestVideoProcessing = this;
            Mototrainer.isProcessingLock.set(false);
            /////////////////////
        }
    }

    @Override
    public void setup() {
        // size(1920, 1080,P2D);
        // frame.setLocation(2920,0); // set to the position of the second display
        Status = SETUP;
        SetupDone = 0;

        gcLogo = new cLogo(MY_PATH + LOGO_IMG);
        fullScreen(P2D, 2);
        // int x = (displayWidth - (int) dSize.sizex) / 2;
        //    int y = (displayHeight - (int) dSize.sizey) / 2;
        gcLogo.iLogo.resize(displayWidth, displayHeight);
        background(gcLogo.iLogo);
    }

    public synchronized void startFromWb() {
        Mototrainer.wbNew.setVisible(false);
        Status = RETURN_FROM_WORKBENCH;
        MOVIE_PATH = WbNew.circuitoSelezionato;
        TRACCIA_MADRE = WbNew.TracciaMadreSelezionata;
        loop();
    }

    public synchronized void exitProcessingFromWb(){
        exit();
    }
    public void startFromWbTracciaDaSalvare(int numGiro) {
        if (numGiro >= 0) {
            cSaveFileClass.saveFile(numGiro, TRACCIA_MADRE);
        }
        Mototrainer.wbNew.setVisible(false);
        Status = RETURN_FROM_WORKBENCH;
        // MOVIE_PATH = WbNew.circuitoSelezionato;
        // TRACCIA_MADRE = WbNew.TracciaMadreSelezionata;
        loop();
    }

    static int counterNothing = 0;

    public void setNumeroGiri(int n) {
        numeroGiri = n;
        contatoreGiri = 0;
    }
    JDialogSaveMasterTrack dialog;

    @Override
    public void draw() {
        int indice, err;
//        print(Status);
//        print(" ");
//        println(cTest);
//        cTest++;
        switch (Status) {
            case SETUP:
                /* -----------------*/
                // fullScreen(P2D, 2);
                subSetup();

                theMovMarciaError = new Movie(this, MY_PATH + MOVIE_PATH_ERR_MARCE) {
                    @Override
                    public void eosEvent() {
                        super.eosEvent();
                        myEoSError();
                    }
                };
                theMovFrenoAntError = new Movie(this, MY_PATH + MOVIE_PATH_ERR_FRENO_ANT) {
                    @Override
                    public void eosEvent() {
                        super.eosEvent();
                        myEoSError();
                    }
                };
                theMovFrenoPosError = new Movie(this, MY_PATH + MOVIE_PATH_ERR_FRENO_POS) {
                    @Override
                    public void eosEvent() {
                        super.eosEvent();
                        myEoSError();
                    }
                };
                theMovAccelError = new Movie(this, MY_PATH + MOVIE_PATH_ERR_ACCEL) {
                    @Override
                    public void eosEvent() {
                        super.eosEvent();
                        myEoSError();
                    }
                };
                theMovAngoloError = new Movie(this, MY_PATH + MOVIE_PATH_ERR_ANGOLO) {
                    @Override
                    public void eosEvent() {
                        super.eosEvent();
                        myEoSError();
                    }
                };

//    theMov.loop();
                // theMov.pause();
//    gcHandleData.updateAllData(0);
                // Status = INIT_PAGE;
                Status = CHKECK_SERIAL;
                
                gcLogo = new cLogo(MY_PATH + LOGO_IMG);
 //               myPort.print((byte)0xf1);

                // timerLogo = millis() + 2000;
//    theMov.pause();
                break;
            case CHKECK_SERIAL:

//                if(Version.buildVersion(popFifo())>=0)
                    Status = INIT_PAGE;
                break;
                
            /* -----------------*/
            case DO_NOTHING:
                myObs.haveToNotify();
//      println("ciao");
                break;
            case INIT_PAGE:
                if (gcLogo.drawLogo() == 0) {
//        closeSerial();
//        noLoop();

                    
                    synchronized(this) {
                    Status = DO_NOTHING;
                    
                     java.awt.EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                Mototrainer.wbNew.setVisible(true);
                    Mototrainer.wbNew.setExtendedState(ICONIFIED);
                    Mototrainer.wbNew.setAlwaysOnTop(true);
                    Mototrainer.wbNew.setExtendedState(MAXIMIZED_BOTH);
                            }
                        });
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
//                    Mototrainer.wbNew.setVisible(true);
//                    Mototrainer.wbNew.setExtendedState(ICONIFIED);
//                    Mototrainer.wbNew.setAlwaysOnTop(true);
//                    Mototrainer.wbNew.setExtendedState(MAXIMIZED_BOTH);
                    // new TestRobotKeys();
//        openSerial(0);

                    /**
                     * test jdialogSaveMasterTrack
                     */
//                    ListaPunti.add(
//                            "giro 1 | err. Freno Anteriore | 1% | err. Freno Posterior | 2% | err. Acceleratore | 3%| err. Inclinazione| 4% | err. Marce | 5% | Errore medio | 6%"
//                    );
//                    ListaPunti.add(
//                            "giro 2 | err. Freno Anteriore | 1% | err. Freno Posterior | 2% | err. Acceleratore | 3%| err. Inclinazione| 4% | err. Marce | 5% | Errore medio | 6%"
//                    );
                    if (ListaPunti.size() > 0) {
                        /* Create and display the dialog */
                        java.awt.EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                dialog = new JDialogSaveMasterTrack(new javax.swing.JFrame(), true, (ArrayList<String>) ListaPunti);
                                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                                    @Override
                                    public void windowClosing(java.awt.event.WindowEvent e) {
                                        //System.exit(0);
                                        Mototrainer.wbNew.setAlwaysOnTop(true);
                                        startFromWbTracciaDaSalvare(JDialogSaveMasterTrack.ndx);
                                    }
                                });
                                Mototrainer.wbNew.setAlwaysOnTop(false);
                                dialog.setVisible(true);
                            }
                        });
                    }
                }

                }
                // System.out.println(counterNothing++);
                break;
            case RETURN_FROM_WORKBENCH:
//                println("A");
                contatoreGiri = 0;
                STopTimer=0;
                ListaPunti.clear();
                theMov = new Movie(this, MY_PATH + MOVIE_PATH) {
                    @Override
                    public void eosEvent() {
                        super.eosEvent();
                        myEoS();
                    }
                };
//                println("B");
                cSaveFileClass.initSaveParameters();
                cErrorClass.resetError();
                tracciaMadre = new leggiFile();
                gcHandleData.updateAllData(0);
//                println("C");
//                theMov.jump(0);
//                theMov.loop();
                gcHandleData.initOldDataTracciaMadre();
                gcPrecount.initPrecount();
                gcHandleData.aggiornaDatiTaratura();
//                 println("C1");
                theMov.loop();
//                println("C2");

                
                cErrorClass.leggiFile(MyProperties.CONF_PATH + "Tolleranze/tolerance.csv");
                cErrorClass.resetError();
                cInfoClass = new InfoClass(WbNew.jTextFieldName.getText(),WbNew.jTextFieldBike.getText(),WbNew.circuitoSelezionato);
                
                Status = READY_TO_START;
                // Status = PRECOUNT;
                //if(millis()<timerLogo) 
                //  gcLogo.drawLogo();
                //else
                //  Status=MASTER_PAGE;
                break;
            case READY_TO_START:
                if (primoFrame != 0) {
                    theMov.jump(0);
//                    println("C3");
                    theMov.pause();
//                    println("D");
                    Status = PRECOUNT;
                    }
                break;
            case PRECOUNT:

                parseData();

                indice = 0;
                if (primoFrame != 0) {
                    noTint();

                    image(theMov, 0, 0, displayWidth, displayHeight);

                    exitButton.drawBottone();
                    if (gcPrecount.displayPrecount() == 0) {
                        EndErroMovie = 0;
                        theMov.play();
                        Status = PLAY;
                        cronTimer=millis();
                    }

                    gcMarce.visMarciaRiferimento();
                    gcMarce.visMarce();
                    gcAnalog.visAnalog();
                    cInfoClass.dispIntestazione();
                    cInfoClass.dispLap(0);
                    cInfoClass.dispTimer(0,0);
                }
                break;
            case PLAY:
                parseData();
                noTint();

                if (millis() > newTimer) {
                    newTimer = millis();
                    gcMarce.timerMarcia();
                }
                //image(theMov, 0, 0);
                image(theMov, 0, 0, width, height);
                // lettura master track
                int t = (int) (theMov.time() * INTERVALLO_MS + 0.5f);
                if (tracciaMadre.nonDisponibile == 0) {
                    gcHandleData.set_Marcetm(t);
                    gcHandleData.set_FrenoAnttm(t);
                    gcHandleData.set_FrenoPostm(t);
                    gcHandleData.set_Acceltm(t);
                    gcHandleData.set_Angolotm(t);
                    gcMarce.visMarciaRiferimento();
                }
                // visualizzzione controlli
                gcMarce.visMarce();
                gcAnalog.visAnalog();
                cInfoClass.dispIntestazione();
                cInfoClass.dispLap(contatoreGiri);
                cInfoClass.dispTimer(millis()-cronTimer,contatoreGiri);
                exitButton.drawBottone();
                cSaveFileClass.storeData(contatoreGiri, t);
                gcPrecount.sfunaGO();
                if (exitButton.testMouse() == 1) {
                    stopVideo();
                    Status = INIT_PAGE;
                } else {
                    err = cErrorClass.timerErrore();
                    if (err < 0) {
                        // si Ã¨ verificato un errore 
                        // theMov.pause();
                        stopVideo();
                        Status = err;
                        // cSaveFileClass.saveFile(1);
                        switch (err) {
                            case M_MARCIA_ERROR:
                                theMovMarciaError.jump(0);
                                theMovMarciaError.play();
                                break;
                            case M_FRENO_POS_ERROR:
                                theMovFrenoPosError.jump(0);
                                theMovFrenoPosError.play();
                                break;
                            case M_FRENO_ANT_ERROR:
                                theMovFrenoAntError.jump(0);
                                theMovFrenoAntError.play();
                                break;
                            case M_ACCEL_ERROR:
                                theMovAccelError.jump(0);
                                theMovAccelError.play();
                                break;
                            case M_ANGOLO_ERROR:
                                theMovAngoloError.jump(0);
                                theMovAngoloError.play();
                                break;
                        }
                    }
                    // gestione contatore giri
                    if (contatoreGiri == numeroGiri) {
                        stopVideo();
                        // cSaveFileClass.generaMatricePunteggio();
                        // theMov.jump(0);

                        Status = INIT_GRAPH;
                        cSaveFileClass.InterpolazioneDati();
                        visualGraph = 0;    // numero del grafico da visualizzare
                    }
                }
                // gestione errori
                break;
            case M_MARCIA_ERROR:
                image(theMovMarciaError, 0, 0, width, height);
                if (EndErroMovie == 1) {
                    // theMov.jump(0);
                    Status = INIT_PAGE;
                }
                break;
            case M_FRENO_ANT_ERROR:
                image(theMovFrenoAntError, 0, 0, width, height);
                if (EndErroMovie == 1) {
                    // theMov.jump(0);
                    Status = INIT_PAGE;
                }
                break;
            case M_FRENO_POS_ERROR:
                image(theMovFrenoPosError, 0, 0, width, height);
                if (EndErroMovie == 1) {
                    // theMov.jump(0);
                    Status = INIT_PAGE;
                }
                break;
            case M_ACCEL_ERROR:
                image(theMovAccelError, 0, 0, width, height);
                if (EndErroMovie == 1) {
                    // theMov.jump(0);
                    Status = INIT_PAGE;
                }
                break;
            case M_ANGOLO_ERROR:
                image(theMovAngoloError, 0, 0, width, height);
                if (EndErroMovie == 1) {
                    // theMov.jump(0);
                    Status = INIT_PAGE;
                }
                break;
            // visualizza grafico
            case INIT_GRAPH:
                xDisegnaGraficoClass = new DisegnaGraficoClass(this, visualGraph, numeroGiri);
                Status = DRAW_GRAPH;
                break;
            case DRAW_GRAPH:
                int x = xDisegnaGraficoClass.drawGraph(visualGraph);
                if (x >= 0) { // premuto bottone
                    switch (x & 0xff00) {
                        case SELECT_BUTTON:
                            visualGraph = x & 0xff;
                            Status = INIT_GRAPH;
                            break;
                        case SAVE_BUTTON:
                            if (fileExits(getMtFilename(MY_PATH + WbNew.circuitoSelezionato))) {
                                saveYesNo.dispDialog();
                                Status = WAIT_FOR_YES_NO;
                            } else {
                                cSaveFileClass.saveFile(visualGraph, getMtFilename(MY_PATH + WbNew.circuitoSelezionato));
                                Status = SAVE_DONE;
                            }

                            // 
                            break;
                        case EXIT_BUTTON:
                            Status = INIT_PAGE;
                            break;
                    }

                }
                break;
            case WAIT_FOR_YES_NO:
                x = saveYesNo.testMouse();
                switch (x) {
                    case BUTTON_YES:
                        cSaveFileClass.saveFile(visualGraph, getMtFilename(MY_PATH + WbNew.circuitoSelezionato));
                        Status = SAVE_DONE;

                        break;
                    case BUTTON_NO:
                        Status = DRAW_GRAPH;
                        break;
                }

                break;
            case SAVE_DONE:
                xDisegnaGraficoClass.drawGraph(visualGraph);
                if (doneMsg.dispDone() == 0) {
                    Status = DRAW_GRAPH;
                }
                break;
        }
    }

    String getMtFilename(String x) {
        for (int i = x.length() - 1; i >= 0; i--) {
            if (x.charAt(i) == '.') {
                x = x.substring(0, i);
                x = x + ".mtk";
                i = -1;
            }
        }
        return x;
    }

    boolean fileExits(String FullFileName) {
        File f = dataFile(FullFileName);
        boolean exist = f.isFile();
        return exist;
    }

    public void stopVideo() {
        theMov.noLoop();
        theMov.stop();
    }

    public void movieEvent(Movie m) {
        if (m != null) {
            m.read();
            primoFrame = 1;
        }
    }

    public void myEoS() {
        contatoreGiri++;
        cronTimer=millis();
        if(contatoreGiri==numeroGiri) {
            STopTimer=1;
        }
            
        // println(contatoreGiri);
    }

    public void myEoSError() {
        EndErroMovie = 1;
    }

    public void keyPressed() {
        if (key == '+') {
            gcMarce.incMarcia();
        }
        if (key == '-') {
            gcMarce.decMarcia();
        }
        if (key >= '1' && key <= '6') {
            int z = PApplet.parseByte(key) - PApplet.parseByte('0');
            gcMarce.setMarciaRiferimento(z);
        }
        if (key == CODED) {
            if (keyCode == UP) {
                gcAnalog.testincFreno();
            }
            if (keyCode == DOWN) {
                gcAnalog.testdecFreno();
            }
        }
        if (DISBLE_ESC) {
            if (key == ESC) {
                key = 0;  // EmpÃªche d'utiliser la touche ESC
            }
        }
    }
//<>// //<>// //<>// //<>//

    static final int M_FRENO_ANT_ERROR = -2; // front brake
    static final int M_FRENO_POS_ERROR = -3; // rear brake
    static final int M_ACCEL_ERROR = -4; // throttle
    static final int M_ANGOLO_ERROR = -5; // lean
    static final int M_MARCIA_ERROR = -6; // gear 

    static final int M_FRENO_ANT = 0; // front brake
    static final int M_FRENO_POS = 1; // rear brake
    static final int M_ACCEL = 2; // throttle
    static final int M_ANGOLO = 3; // lean
    static final int M_MARCE = 4; // gear

    public class ErrorClass {

        static final String S_FRENO_ANT = "FRONTBRAKE"; // front brake
        static final String S_FRENO_POS = "REARBRAKE"; // rear brake
        static final String S_ACCELL = "THROTTLE"; // throttle
        static final String S_ANGOLO = "LEAN"; // lean
        static final String S_MARCE = "GEAR"; // gear
        static final String S_TIMETOGAMEOVER = "TIMETOGAMEOVER"; // Time to Game Over

        static final int BEGINNER = 1;
        static final int INTERMEDIATE = 3;
        static final int EXPERT = 5;
        static final int RIDER = 7;

        static final int BEGINNER_AV = 2;
        static final int INTERMEDIATE_AV = 4;
        static final int EXPERT_AV = 6;
        static final int RIDER_AV = 8;

        static final int M_BEGINNER = 0;
        static final int M_INTERMEDIATE = 1;
        static final int M_EXPERT = 2;
        static final int M_RIDER = 3;

        String[] pieces;
        float[][] MatTolleran = new float[M_MARCE + 1][M_RIDER + 1];   // stringa per la lettura file
        boolean[][] MatAvviso = new boolean[M_MARCE + 1][M_RIDER + 1];   // stringa per la lettura file
        int[] TimeToGameOver = new int[M_RIDER + 1]; // temporizzazione x timeout

        int SkillLEvel;
        int TimerMarce, AvvisoMarce;
        int TimerFrenoAnt, AvvisoFrenoAnt;
        int TimerFrenoPos, AvvisoFrenoPos;
        int TimerAccel, AvvisoAccel;
        int TimerAngolo, AvvisoAngolo;

        public void resetError() {
            TimerMarce = -1;
            AvvisoMarce = 0;
            TimerFrenoAnt = -1;
            AvvisoFrenoAnt = 0;
            TimerFrenoPos = -1;
            AvvisoFrenoPos = 0;
            TimerAccel = -1;
            AvvisoAccel = 0;
            TimerAngolo = -1;
            AvvisoAngolo = 0;
        }

        public void leggiFile(String Filename) {
            String line = null;
            int i;
            if(fileExits(Filename)==false)
                return;
            BufferedReader reader = createReader(Filename);
            i = 0;
            try {
                while ((line = reader.readLine()) != null) {
                    pieces = split(line, "\t");
                    if (pieces.length == 1) {
                        pieces = split(line, ",");
                    }
                    pieces[0] = RmExtra(pieces[0]);
                    switch (pieces[0]) {
                        case S_FRENO_ANT:
                            storeInMat(M_FRENO_ANT);
                            break;
                        case S_FRENO_POS:
                            storeInMat(M_FRENO_POS);
                            break;
                        case S_ACCELL:
                            storeInMat(M_ACCEL);
                            break;
                        case S_ANGOLO:
                            storeInMat(M_ANGOLO);
                            break;
                        case S_MARCE:
                            storeInMat(M_MARCE);
                            break;
                        case S_TIMETOGAMEOVER:
                            TimeToGameOver[M_BEGINNER] = reatTime(BEGINNER);
                            TimeToGameOver[M_INTERMEDIATE] = reatTime(INTERMEDIATE);
                            TimeToGameOver[M_EXPERT] = reatTime(EXPERT);
                            TimeToGameOver[M_RIDER] = reatTime(RIDER);
                    }
                    i++;
                }
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public int reatTime(int colonna) {
            String tmpStr;
            int t;
            t = 0;
            tmpStr = RmExtra(pieces[colonna]);
            tmpStr = tryToInt(tmpStr);
            t = Integer.parseInt(tmpStr) * 1000;
            return t;
        }

        public void storeInMat(int riga) {
            MatTolleran[riga][M_BEGINNER] = readVal(pieces[BEGINNER]);
            MatTolleran[riga][M_INTERMEDIATE] = readVal(pieces[INTERMEDIATE]);
            MatTolleran[riga][M_EXPERT] = readVal(pieces[EXPERT]);
            MatTolleran[riga][M_RIDER] = readVal(pieces[RIDER]);

            MatAvviso[riga][M_BEGINNER] = readAvviso(pieces[BEGINNER_AV]);
            MatAvviso[riga][M_INTERMEDIATE] = readAvviso(pieces[INTERMEDIATE_AV]);
            MatAvviso[riga][M_EXPERT] = readAvviso(pieces[EXPERT_AV]);
            MatAvviso[riga][M_RIDER] = readAvviso(pieces[RIDER_AV]);
        }

        public boolean readAvviso(String s) {
            String tmpStr;
            boolean f;
            f = false;
            tmpStr = RmExtra(s);
            if (tmpStr != "") {
                f = true;
            }
            return f;
        }

            public float readVal(String p) {
            String tmpStr;
            float f;
            tmpStr = RmExtra(p);
            f = Float.parseFloat(tmpStr);
            return f;
        }
        
//        public float readPercentuale(String p) {
//            String tmpStr;
//            float f;
//            tmpStr = RmExtra(p);
//            f = Float.parseFloat(tmpStr) / 100;
//            return f;
//        }

        public String RmExtra(String str) {
            String retStr;
            char c;
            int i;
            int j;
            str = str.toUpperCase();
            retStr = "";
            for (i = 0; i < str.length(); i++) {
                c = str.charAt(i);
                if (c >= 'A' && c <= 'Z') {
                    retStr = retStr + c;
                }
                if (c >= '0' && c <= '9') {
                    retStr = retStr + c;
                }
                if (c == '.' || c == ',') {
                    c = '.';
                    retStr = retStr + c;
                }
            }
            return retStr;
        }

        public String tryToInt(String str) {
            String retStr;
            char c;
            int i;
            int j;
            str = str.toUpperCase();
            retStr = "";
            for (i = 0; i < str.length(); i++) {
                c = str.charAt(i);
                if (c >= '0' && c <= '9') {
                    retStr = retStr + c;
                }
            }
            if (retStr == "") {
                retStr = "-1";
            }
            return retStr;
        }

        public void setSkillLEvel(int l) {
            SkillLEvel = l;
            // System.out.println("Skillevel set to "+l);
        }

        public int getSkillLEvel() {
            return SkillLEvel;
        }

        public void setErroreMarce() {
            if (Status == PLAY) {
                PlayAudioMarceError();
                if (TimerMarce < 0) {
                    if (TimeToGameOver[SkillLEvel] > 0) { // situazione non BEGINNER
                        TimerMarce = millis() + TimeToGameOver[SkillLEvel];
                    }
                }
            }
        }

        public void stopErroreMarce() {
            TimerMarce = -1;
        }

        public int timerErrore() {
            int ris = 0;
            if (TimerMarce >= 0) {
                if (TimerMarce < millis()) {
                    ris = M_MARCIA_ERROR;
                }
            }
            if (TimerFrenoAnt >= 0) {
                if (TimerFrenoAnt < millis()) {
                    ris = M_FRENO_ANT_ERROR;
                }
            }
            if (TimerFrenoPos >= 0) {
                if (TimerFrenoPos < millis()) {
                    ris = M_FRENO_POS_ERROR;
                }
            }
            if (TimerAccel >= 0) {
                if (TimerAccel < millis()) {
                    ris = M_ACCEL_ERROR;
                }
            }
            if (TimerAngolo >= 0) {
                if (TimerAngolo < millis()) {
                    ris = M_ANGOLO_ERROR;
                }
            }

            return ris;
        }

        public void testErrorFrenoAnt() {
            // controlla se si Ã¨ nello status PLAY
            float estremoSup;
            float estremoInf;
            if (Status == PLAY) {
                if (TimerFrenoAnt < 0 && tracciaMadre.nonDisponibile == 0) {
                    //  estremoSup = MatTolleran[M_FRENO_ANT][SkillLEvel] * (float) gcAnalog.analogFrenoAnt_tm + (float) gcAnalog.analogFrenoAnt_tm;
                    //  estremoInf = (float) gcAnalog.analogFrenoAnt_tm - MatTolleran[M_FRENO_ANT][SkillLEvel] * (float) gcAnalog.analogFrenoAnt_tm;
                    estremoSup = MatTolleran[M_FRENO_ANT][SkillLEvel] + (float) gcAnalog.analogFrenoAnt_tm;
                    estremoInf = (float) gcAnalog.analogFrenoAnt_tm - MatTolleran[M_FRENO_ANT][SkillLEvel];
                    if (gcAnalog.analogFrenoAnt > estremoSup || gcAnalog.analogFrenoAnt < estremoInf) {
                        if (AvvisoFrenoAnt == 0) {
                            PlayAudioFrenoAntError();
                            gcAnalog.err_FrenoAnt = 1;
                            AvvisoFrenoAnt = 1;
                        }
                        if (MatAvviso[M_FRENO_ANT][SkillLEvel] == false) { // nel caso true dare solo avviso senza settare timer errore
                            if (TimeToGameOver[SkillLEvel] > 0) { // situazione non BEGINNER
                                TimerFrenoAnt = millis() + TimeToGameOver[SkillLEvel];
                            }
                        }
                    } else {
                        TimerFrenoAnt = -1;  // reset error
                        AvvisoFrenoAnt = 0;
                        gcAnalog.err_FrenoAnt = 0;
                    }
                }
            }
        }

        float calcolaestremoSup(float dato, int indice) {
            float estremoSup;
            // metodo lineare
            // estremoSup = MatTolleran[indice][SkillLEvel] * dato + dato;

            // metodo non lineare
            estremoSup = MatTolleran[indice][SkillLEvel] + dato;
            return estremoSup;
        }

        float calcolaestremoInf(float dato, int indice) {
            float estremoInf;
            // metodo lineare
            // estremoInf = dato - MatTolleran[indice][SkillLEvel] * dato;

            // metodo non lineare
            estremoInf = MatTolleran[indice][SkillLEvel] - dato;
            return estremoInf;
        }

        public void testErrorFrenoPos() {
            // controlla se si Ã¨ nello status PLAY
            float estremoSup;
            float estremoInf;
            if (Status == PLAY) {
                if (TimerFrenoAnt < 0 && tracciaMadre.nonDisponibile == 0) {
//                    estremoSup = MatTolleran[M_FRENO_POS][SkillLEvel] * (float) gcAnalog.analogFrenoPos_tm + (float) gcAnalog.analogFrenoPos_tm;
//                    estremoInf = (float) gcAnalog.analogFrenoPos_tm - MatTolleran[M_FRENO_POS][SkillLEvel] * (float) gcAnalog.analogFrenoPos_tm;
                    estremoSup = MatTolleran[M_FRENO_POS][SkillLEvel] + (float) gcAnalog.analogFrenoPos_tm;
                    estremoInf = (float) gcAnalog.analogFrenoPos_tm - MatTolleran[M_FRENO_POS][SkillLEvel];
                    if (gcAnalog.analogFrenoPos > estremoSup || gcAnalog.analogFrenoPos < estremoInf) {
                        if (AvvisoFrenoPos == 0) {
                            PlayAudioFrenoPosError();
                            AvvisoFrenoPos = 1;
                            gcAnalog.err_FrenoPos = 1;

                        }
                        if (MatAvviso[M_FRENO_POS][SkillLEvel] == false) { // nel caso true dare solo avviso senza settare timer errore
                            if (TimeToGameOver[SkillLEvel] > 0) { // situazione non BEGINNER
                                TimerFrenoPos = millis() + TimeToGameOver[SkillLEvel];
                            }
                        }
                    } else {
                        TimerFrenoPos = -1;  // reset error
                        AvvisoFrenoPos = 0;
                        gcAnalog.err_FrenoPos = 0;
                    }
                }
            }
        }

        public void testErrorAccel() {
            // controlla se si Ã¨ nello status PLAY
            float estremoSup;
            float estremoInf;
            if (Status == PLAY) {
                if (TimerAccel < 0 && tracciaMadre.nonDisponibile == 0 ) {
//                    estremoSup = MatTolleran[M_ACCEL][SkillLEvel] * (float) gcAnalog.analogAccel_tm + (float) gcAnalog.analogAccel_tm;
//                    estremoInf = (float) gcAnalog.analogAccel_tm - MatTolleran[M_ACCEL][SkillLEvel] * (float) gcAnalog.analogAccel_tm;
                    estremoSup = MatTolleran[M_ACCEL][SkillLEvel] + (float) gcAnalog.analogAccel_tm;
                    estremoInf = (float) gcAnalog.analogAccel_tm - MatTolleran[M_ACCEL][SkillLEvel];
                    if (gcAnalog.analogAccel > estremoSup || gcAnalog.analogAccel < estremoInf) {
                        if (AvvisoAccel == 0) {
                            PlayAudioAccelError();
                            AvvisoAccel = 1;
                            gcAnalog.err_Accel = 1;

                        }
                        if (MatAvviso[M_ACCEL][SkillLEvel] == false) { // nel caso true dare solo avviso senza settare timer errore
                            if (TimeToGameOver[SkillLEvel] > 0) { // situazione non BEGINNER
                                TimerAccel = millis() + TimeToGameOver[SkillLEvel];
                            }
                        }
                    } else {
                        TimerAccel = -1;  // reset error
                        AvvisoAccel = 0;
                        gcAnalog.err_Accel = 0;
                    }
                }
            }
        }

        public void testErrorAngolo() {
            // controlla se si Ã¨ nello status PLAY
            float estremoSup;
            float estremoInf;
            if (Status == PLAY) {
                if (TimerAngolo < 0 && tracciaMadre.nonDisponibile == 0) {
//                    estremoSup = MatTolleran[M_ANGOLO][SkillLEvel] * (float) gcAnalog.analogAngolo_tm + (float) gcAnalog.analogAngolo_tm;
//                    estremoInf = (float) gcAnalog.analogAngolo_tm - MatTolleran[M_ANGOLO][SkillLEvel] * (float) gcAnalog.analogAngolo_tm;
                    estremoSup = MatTolleran[M_ANGOLO][SkillLEvel] + (float) gcAnalog.analogAngolo_tm;
                    estremoInf = (float) gcAnalog.analogAngolo_tm - MatTolleran[M_ANGOLO][SkillLEvel];
                    if (gcAnalog.analogAngolo > estremoSup || gcAnalog.analogAngolo < estremoInf) {
                        if (AvvisoAngolo == 0) {
                            PlayAudioAngoloError();
                            AvvisoAngolo = 1;
                            gcAnalog.err_Angolo = 1;
                        }
                        if (MatAvviso[M_ANGOLO][SkillLEvel] == false) { // nel caso true dare solo avviso senza settare timer errore
                            if (TimeToGameOver[SkillLEvel] > 0) { // situazione non BEGINNER
                                TimerAngolo = millis() + TimeToGameOver[SkillLEvel];
                            }
                        }
                    } else {
                        TimerAngolo = -1;  // reset error
                        AvvisoAngolo = 0;
                        gcAnalog.err_Angolo = 0;
                    }
                }
            }
        }
    }

    AudioPlayer audioErrMArcia;
    AudioPlayer audioErrFrenoAnt;
    AudioPlayer audioErrFrenoPos;
    AudioPlayer audioErrAngolo;
    AudioPlayer audioErrAccel;

    Minim minim;

    public void loadAudioError() {
        minim = new Minim(this);
        audioErrMArcia = minim.loadFile(MyProperties.CONF_PATH + "\\errorSound\\gearError.wav");
        audioErrAngolo = minim.loadFile(MyProperties.CONF_PATH + "/errorSound/leanError.wav");
        audioErrAccel = minim.loadFile(MyProperties.CONF_PATH + "/errorSound/throttleError.wav");
        audioErrFrenoAnt = minim.loadFile(MyProperties.CONF_PATH + "/errorSound/frontbrakeError.wav");
        audioErrFrenoPos = minim.loadFile(MyProperties.CONF_PATH + "/errorSound/rearbrakeError.wav");
    }

    public void PlayAudioMarceError() {
        audioErrMArcia.rewind();
        audioErrMArcia.play();
    }

    public void PlayAudioFrenoAntError() {
        audioErrFrenoAnt.rewind();
        audioErrFrenoAnt.play();
    }

    public void PlayAudioFrenoPosError() {
        audioErrFrenoPos.rewind();
        audioErrFrenoPos.play();
    }

    public void PlayAudioAccelError() {
        audioErrAccel.rewind();
        audioErrAccel.play();
    }

    public void PlayAudioAngoloError() {
        audioErrAngolo.rewind();
        audioErrAngolo.play();
    }

    class PrecountClass {

        PImage[] iPrecount = new PImage[4];
        int[] x = new int[4];
        int[] y = new int[4];
        int timer;
        int Counter;
        int oldCounter;
        int sfTimer;
        int myTint;

        PrecountClass() {
            iPrecount[3] = loadImage(MyProperties.CONF_PATH + "Grafica/Precount/3.png");
            iPrecount[2] = loadImage(MyProperties.CONF_PATH + "Grafica/Precount/2.png");
            iPrecount[1] = loadImage(MyProperties.CONF_PATH + "Grafica/Precount/1.png");
            iPrecount[0] = loadImage(MyProperties.CONF_PATH + "Grafica/Precount/go.png");

            for (int i = 0; i < 4; i++) {
                x[i] = (displayWidth - iPrecount[i].width) / 2;
                y[i] = (displayHeight - iPrecount[i].height) / 2;
            }
            initPrecount();
        }

        public void initPrecount() {
            timer = -1;
            Counter = 3;
            oldCounter = -1;
            myTint = 255;
        }

        public int displayPrecount() {
            if (oldCounter != Counter) {
                oldCounter = Counter;
                timer = millis() + 1000;
            } else {
                if (millis() >= timer) {
                    Counter--;
                }
            }
            image(iPrecount[Counter], x[Counter], y[Counter]);
            sfTimer = millis() + 10;
            return Counter;
        }

        public void sfunaGO() {
            if (sfTimer >= 0) {
                tint(255, myTint);
                image(iPrecount[Counter], x[Counter], y[Counter]);
                noTint();
                if (sfTimer <= millis()) {
                    sfTimer = millis() + 10;
                    myTint = myTint - 2;
                    if (myTint < 0) {
                        sfTimer = -1;
                    }
                }
            }
        }
    }

    /* salvataggio master track */ //<>// //<>// //<>//
    static final int NUM_GIRI = 10;

    class SaveFileClass {

        int[][] tmMarce;
        int[][] tmFrenoAnt;
        int[][] tmFrenoPos;
        int[][] tmAccel;
        int[][] tmAngolo;
        int[][] recordArray;
        int[] recordIndex;
        int[] numRecords;
        int numRecord;
        int nGiri = 0;

        PrintWriter output;

        SaveFileClass() {

            tmMarce = new int[NUM_GIRI][NUM_RECORD];
            tmFrenoAnt = new int[NUM_GIRI][NUM_RECORD];
            tmFrenoPos = new int[NUM_GIRI][NUM_RECORD];
            tmAccel = new int[NUM_GIRI][NUM_RECORD];
            tmAngolo = new int[NUM_GIRI][NUM_RECORD];
            recordArray = new int[NUM_GIRI][NUM_RECORD];
            recordIndex = new int[NUM_RECORD];
            numRecords = new int[NUM_GIRI];
        }

        int oldMarce = 0;
        int oldFrenoAnt = 0;
        int oldFrenoPos = 0;
        int oldAcel = 0;
        int oldAngolo = 0;

        /* --------------------------------------------- */
        public int getValues(int valIndex, int giro, int n) {
            int retV = 0;
            switch (valIndex) {
                case M_FRENO_ANT:
                    retV = getFrenoAnt(giro, n);
                    break;
                case M_FRENO_POS:
                    retV = getFrenoPos(giro, n);
                    break;
                case M_ACCEL:
                    retV = getAccel(giro, n);
                    break;
                case M_ANGOLO:
                    retV = getAngolo(giro, n);
                    break;
                case M_MARCE:
                    retV = getMarce(giro, n);
                    break;
            }
            return retV;
        }

        public int getFrenoAnt(int giro, int n) {
            if (tmFrenoAnt[giro][n] >= 0) {
                oldFrenoAnt = tmFrenoAnt[giro][n];
            }
            return oldFrenoAnt;
        }

        public int getFrenoPos(int giro, int n) {
            if (tmFrenoPos[giro][n] >= 0) {
                oldFrenoPos = tmFrenoPos[giro][n];
            }
            return oldFrenoPos;
        }

        public int getAccel(int giro, int n) {
            if (tmAccel[giro][n] >= 0) {
                oldAcel = tmAccel[giro][n];
            }
            return oldAcel;
        }

        public int getMarce(int giro, int n) {
            if (tmMarce[giro][n] >= 0) {
                oldMarce = tmMarce[giro][n];
            }
            return oldMarce;
        }

        public int getAngolo(int giro, int n) {
            if (tmAngolo[giro][n] <= 90 && tmAngolo[giro][n] >= -90) {
                oldAngolo = tmAngolo[giro][n];
            }
            return oldAngolo;
        }

        /* --------------------------------------------- */
        public void initSaveParameters() {
            int i, j;

            for (i = 0; i < NUM_GIRI; i++) {
                for (j = 0; j < NUM_RECORD; j++) {
                    tmMarce[i][j] = -1;     // 1
                    tmFrenoAnt[i][j] = -1;  // 2
                    tmFrenoPos[i][j] = -1;  // 3
                    tmAccel[i][j] = -1;     // 4
                    tmAngolo[i][j] = -1;    // 5
                    recordArray[i][j] = -1;
                }
                recordIndex[i] = 0;
                numRecords[i]=-1;
            }

            numRecord = -1;
            nGiri = 0;

        }

        public void visualizzaGrafico() {
            int nStep;
            float inc;
            int x = 0;
            int y = 0;
            int bordoInf = 100; // spazion tra l'asse ascisse ed il fondo
            int bordoSx = 100; // spazio fra l'asse ordinate ed il margine a sx
            int bordoDx = 100; // spazio fra l'asse ordinate ed il margine a sx
            int bordoSup = 50;    // margine superiore del grafico;
            fill(255, 255, 0);
            rect(0, 0, displayWidth, displayHeight);
            // disegna assi
            fill(255, 255, 255);
            strokeWeight(1);
            y = displayHeight - bordoInf;
            line(bordoSx, y, displayWidth - bordoDx, y);
            y = displayHeight - bordoInf - 100;
            strokeWeight(2);
            line(bordoSx, y, displayWidth - bordoDx, y);
            InterpolazioneDati();
            nStep = contaDati();
            inc = (float) displayWidth / (float) nStep;

            // 
        }

        void disegnaRectSfondo(int x0, int y0, int x1, int y1) {

        }

        public void generaMatricePunteggio() {
            InterpolazioneDati();
            // la matrice risultati viene generata leggendo i dati della maaster track 
            // e confrontandoli con quelli registrati su ogni giro.
            // va calcolata la percentuale di scostamento tra il valore master
            // e quello letto. 
            // scostamento = valore assoluto di (Master - valore)
            //  estremoSup = MatTolleran[M_FRENO_ANT][SkillLEvel] * (float) gcAnalog.analogFrenoAnt_tm + (float) gcAnalog.analogFrenoAnt_tm;
            //  estremoInf = (float) gcAnalog.analogFrenoAnt_tm - MatTolleran[M_FRENO_ANT][SkillLEvel] * (float) gcAnalog.analogFrenoAnt_tm;
            // diff = estremoSup - estremoInf
            // 100 : diff = x : valore
            // x = (valore * 100) / diff
            float mediaFrenoAnt = 0;
            float mediaFrenoPos = 0;
            float mediaAccel = 0;
            float mediaAngolo = 0;
            float mediaMarcia = 0;
            int j;
            float estremoSup;
            float estremoInf;
            float dato;
            float diff;
            int tContatore = 0;
            int numGiro = 0;
            // abbiamo 2 casi:
            // 1 - caso in cui la traccia madre esiste
            // 2 - caso in cui la traccia madre non esiste

            if (tracciaMadre.nonDisponibile == 0) {
                // 1
                for (numGiro = 0; numGiro < NUM_GIRI; numGiro++) {
                    mediaFrenoAnt = 0;
                    mediaFrenoPos = 0;
                    mediaAccel = 0;
                    mediaAngolo = 0;
                    mediaMarcia = 0;
                    j = 0;
                    while (tmFrenoAnt[numGiro][j] >= 0) {   // tmFrenoAnt contiene i dati letti da Arduino
                        // legge dato traccia madre

                        // FRENO ANTERIORE
                        dato = tracciaMadre.getSafeFrenoAnt(j);
                        estremoSup = cErrorClass.calcolaestremoSup(dato, M_FRENO_ANT);
                        estremoInf = cErrorClass.calcolaestremoInf(dato, M_FRENO_ANT);
                        diff = estremoSup - estremoInf;
                        mediaFrenoAnt = mediaFrenoAnt + (tmFrenoAnt[numGiro][j] * 100) / diff;

                        // FRENO POSTERIORE
                        dato = tracciaMadre.getSafeFrenoPos(j);
                        estremoSup = cErrorClass.calcolaestremoSup(dato, M_FRENO_POS);
                        estremoInf = cErrorClass.calcolaestremoInf(dato, M_FRENO_POS);
                        diff = estremoSup - estremoInf;
                        mediaFrenoPos = mediaFrenoPos + (tmFrenoPos[numGiro][j] * 100) / diff;

                        // Acceleratore
                        dato = tracciaMadre.getSafeAccel(j);
                        estremoSup = cErrorClass.calcolaestremoSup(dato, M_ACCEL);
                        estremoInf = cErrorClass.calcolaestremoInf(dato, M_ACCEL);
                        diff = estremoSup - estremoInf;
                        mediaAccel = mediaAccel + (tmAccel[numGiro][j] * 100) / diff;

                        // Anngolo
                        dato = tracciaMadre.getSafeAngolo(j);    // 0 -180
                        estremoSup = cErrorClass.calcolaestremoSup(dato, M_ANGOLO);
                        estremoInf = cErrorClass.calcolaestremoInf(dato, M_ANGOLO);
                        diff = estremoSup - estremoInf;
                        mediaAngolo = mediaAngolo + (tmAngolo[numGiro][j] * 100) / diff;

                        // marcia
                        // la marcia non ha un riferimento di valore puntuale
                        // quindi se la differebza Ã¨ !=0 va calcolato per quanto tempo questa risulta differente.
                        dato = tracciaMadre.getSafeMarcia(j);
                        diff = dato - tmMarce[numGiro][j];
                        if (diff != 0) {
                            tContatore++;   // conta le decine di millisecondi
                        } else {
                            // calcola area
                            int area = 0;
                            for (int k = j - tContatore; k < j; k++) {
                                dato = tracciaMadre.getSafeMarcia(k) - tmMarce[numGiro][k];
                                if (dato < 0) {
                                    dato = -dato;
                                }
                                area = area + (int) dato;
                                mediaMarcia = mediaMarcia + area;
                            }

                            tContatore = 0;
                        }
                        j++;
                    }
                    mediaFrenoAnt = mediaFrenoAnt / j;
                    mediaFrenoPos = mediaFrenoPos / j;
                    mediaAccel = mediaAccel / j;
                    mediaAngolo = mediaAngolo / j;
                    mediaMarcia = mediaMarcia / j;
                    // genera array lis x mMariano
                    String elemento = "Giro " + (numGiro + 1) + SEPARATORE;

                    elemento = elemento + "err. Front brake" + SEPARATORE;
                    elemento = elemento + String.format("%2.2f", mediaFrenoAnt) + SEPARATORE;

                    elemento = elemento + "err. Rear brake" + SEPARATORE;
                    elemento = elemento + String.format("%2.2f", mediaFrenoPos) + SEPARATORE;

                    elemento = elemento + "err. Throttle" + SEPARATORE;
                    elemento = elemento + String.format("%2.2f", mediaAccel) + SEPARATORE;

                    elemento = elemento + "err. Gear" + SEPARATORE;
                    elemento = elemento + String.format("%2.2f", mediaMarcia) + SEPARATORE;

                    elemento = elemento + "err. Lean" + SEPARATORE;
                    elemento = elemento + String.format("%2.2f", mediaAngolo);

                    ListaPunti.add(elemento);
                }
            } else {
                for (numGiro = 0; numGiro < NUM_GIRI; numGiro++) {

                    String elemento = "Giro " + (numGiro + 1) + SEPARATORE;

                    elemento = elemento + SEPARATORE;
                    elemento = elemento + SEPARATORE;

                    elemento = elemento + SEPARATORE;
                    elemento = elemento + SEPARATORE;

                    elemento = elemento + SEPARATORE;
                    elemento = elemento + SEPARATORE;

                    elemento = elemento + SEPARATORE;
                    elemento = elemento + SEPARATORE;

                    elemento = elemento + SEPARATORE;
                    elemento = elemento + " ";

                    ListaPunti.add(elemento);
                }
            }
        }

        public void storeData(int numGiro, int time) {
            // numGiro--;
            if (numGiro < NUM_GIRI) {
                if (time >= numRecords[numGiro]) {
                    nGiri = numGiro + 1;
                    if (time < NUM_RECORD) {
                        // println(time);
                        tmMarce[numGiro][time] = gcMarce.nextMarcia + 1;
                        tmFrenoAnt[numGiro][time] = gcAnalog.analogFrenoAnt;
                        tmFrenoPos[numGiro][time] = gcAnalog.analogFrenoPos;
                        tmAccel[numGiro][time] = gcAnalog.analogAccel;
                        tmAngolo[numGiro][time] = gcAnalog.angoloxFile;
                        if (numRecord != time) {
                            numRecord = time;
                            recordArray[numGiro][recordIndex[numGiro]] = numRecord;
                            recordIndex[numGiro]++;
                            numRecords[numGiro] = numRecord;
                        }
                    }
                }
            }
        }

        int contaDati() {
            // conta i dati validi calcolandoli sulla 
            // registrazione delle marce nel primo giro.
            int i;
            i = 0;
            while (tmMarce[0][i] >= 0) {
                i++;
            }
            return i;
        }

        public void InterpolazioneDati() {
            int i;
            int j;
            int dx, dy;
            int x1, x2;
            int y1, y2;
            int x, y;
            float m, n;
            for (int numGiro = 0; numGiro < NUM_GIRI; numGiro++) {
                // trova primo record utile e aggiunge
                j = recordArray[numGiro][0];
                if (j >= 0) {

                    for (i = j - 1; i >= 0; i--) {
                        tmMarce[numGiro][i] = tmMarce[numGiro][j];
                        tmFrenoAnt[numGiro][i] = tmFrenoAnt[numGiro][j];
                        tmFrenoPos[numGiro][i] = tmFrenoPos[numGiro][j];
                        tmAccel[numGiro][i] = tmAccel[numGiro][j];
                        tmAngolo[numGiro][i] = tmAngolo[numGiro][j];
                    }

                    for (i = 1; i < recordIndex[numGiro]; i++) {
                        // interpola dati mancanti
                        x1 = recordArray[numGiro][i - 1];
                        x2 = recordArray[numGiro][i];
                        if (x2 > x1) {
                            // marce
                            y1 = tmFrenoAnt[numGiro][x1];
                            for (x = x1; x < x2; x++) {
                                y = y1;
                                tmFrenoAnt[numGiro][x] = y;
                            }
                            // freno ant
                            y1 = tmFrenoAnt[numGiro][x1];
                            y2 = tmFrenoAnt[numGiro][x2];
                            m = (float) (y2 - y1) / PApplet.parseFloat(x2 - x1);
                            n = (float) y1 - (float) ((float) (y2 - y1) * (float) x1) / (float) (x2 - x1);
                            for (x = x1; x < x2; x++) {
                                y = (int) ((m * x) + n + 0.5f);
                                tmFrenoAnt[numGiro][x] = y;
                            }

                            // freno post
                            y1 = tmFrenoPos[numGiro][x1];
                            y2 = tmFrenoPos[numGiro][x2];
                            m = (float) (y2 - y1) / PApplet.parseFloat(x2 - x1);
                            n = (float) y1 - (float) ((float) (y2 - y1) * (float) x1) / (float) (x2 - x1);
                            for (x = x1; x < x2; x++) {
                                y = (int) ((m * x) + n + 0.5f);
                                tmFrenoPos[numGiro][x] = y;
                            }

                            // accel
                            y1 = tmAccel[numGiro][x1];
                            y2 = tmAccel[numGiro][x2];
                            m = (float) (y2 - y1) / PApplet.parseFloat(x2 - x1);
                            n = (float) y1 - (float) ((float) (y2 - y1) * (float) x1) / (float) (x2 - x1);
                            for (x = x1; x < x2; x++) {
                                y = (int) ((m * x) + n + 0.5f);
                                tmAccel[numGiro][x] = y;
                            }

                            // angolo
                            y1 = tmAngolo[numGiro][x1];
                            y2 = tmAngolo[numGiro][x2];
                            m = (float) (y2 - y1) / PApplet.parseFloat(x2 - x1);
                            n = (float) y1 - (float) ((float) (y2 - y1) * (float) x1) / (float) (x2 - x1);
                            for (x = x1; x < x2; x++) {
                                y = (int) ((m * x) + n + 0.5f);
                                tmAngolo[numGiro][x] = y;
                            }
                        }
                    }
                }
            }
        }

        public void saveFile(int numGiro, String Filename) {
            int i;
            output = createWriter(Filename);
            //    final static int MARCE_POS=0;
            //    final static int FRENOANT_POS=1;
            //    final static int FRENOPOS_POS=2;
            //    final static int ACCEL_POS=3;
            //    final static int ANGOLO_POS=4;
            for (i = 0; i < numRecords[numGiro]; i++) {
                output.print(tmMarce[numGiro][i] + "\t");
                output.print(tmFrenoAnt[numGiro][i] + "\t");
                output.print(tmFrenoPos[numGiro][i] + "\t");
                output.print(tmAccel[numGiro][i] + "\t");
                output.println(tmAngolo[numGiro][i]);
            }
            output.flush(); // Writes the remaining data to the file
            output.close(); // Finishes the file
        }
    }

    static final int MORBIDELLI_COLOR = 0xff0cb040; // verde
    static final int RABAT_COLOR = 0xffe84d1b;    // arancione
    static final int ROSSI_COLOR = 0xffffff00;    // giallo
    static final int VINALES_COLOR = 0xffed2028;    // rosso
    static final int ESPARGARO_COLOR = 0xffe14e95;    // viola
    static final int DOVIZIOSO_COLOR = 0xff6d9dc9;    // celeste

    static final int SAVE_BUTTON = 0x1000;
    static final int SELECT_BUTTON = 0x2000;
    static final int EXIT_BUTTON = 0x3000;
    static final int BUTTON_YES = 0x4000;
    static final int BUTTON_NO = 0x5000;

    public class DisegnaGraficoClass {

        int[] xp;
        int[] yp;

        int altezzaRect;
        int larghezzaRect;

        int xz = 20;  // spazio margine sx,margine dx
        int yUP = 40;  // spazio sopra
        int yDN = 20;
        // int yyz = 0;
        int dy = 10;  // sozeio DY tra rettangoli

        PImage Sfondo;
        PImage reportTitle;
        PImage lap;

        GPlot[] plots;

        String[] GraphTitle;

        PImage[] labels;
        String[] labelTiles;

        PImage saveMasterTrack;

        int[] myColor;

        RadioButtonClass buttons;
        cBottone buttonExit;
        cBottone buttonSave;

        int oldMouseButton;
        GPointsArray[] points;
        GPointsArray[] points_mt;

        //          yz
        //     +------------------+
        //  xz |          0       | xz
        //     +------------------+
        //          dy
        //     +------------------+
        //     |          1       |
        //     +------------------+
        //          yyz
        DisegnaGraficoClass(PApplet parent, int Giro, int lnGiri) {
            xp = new int[5];
            yp = new int[5];

            myColor = new int[5];
            int nPunti = cSaveFileClass.numRecords[Giro];
            int nGiri = lnGiri;

            int[] posArray;
            posArray = new int[5];

            points = new GPointsArray[5]; //(parent);

            
            points_mt = new GPointsArray[5]; //(parent);

            labels = new PImage[5];
            labelTiles = new String[5];

            posArray[0] = M_FRENO_ANT;
            posArray[1] = M_FRENO_POS;
            posArray[2] = M_ACCEL;
            posArray[3] = M_ANGOLO;
            posArray[4] = M_MARCE;

            labelTiles[posArray[0]] = "lblFrontBrake.png";
            labelTiles[posArray[1]] = "lblRearBrake.png";
            labelTiles[posArray[2]] = "lblThrottle.png";
            labelTiles[posArray[3]] = "lblLean.png";
            labelTiles[posArray[4]] = "lblGear.png";

            

            myColor[0] = RABAT_COLOR;
            myColor[1] = ROSSI_COLOR;
            myColor[2] = DOVIZIOSO_COLOR;
            myColor[3] = ESPARGARO_COLOR;
            myColor[4] = MORBIDELLI_COLOR;

            points[posArray[0]] = new GPointsArray(nPunti);
            points[posArray[1]] = new GPointsArray(nPunti);
            points[posArray[2]] = new GPointsArray(nPunti);
            points[posArray[3]] = new GPointsArray(nPunti);
            points[posArray[4]] = new GPointsArray(nPunti);

            points_mt[posArray[0]] = new GPointsArray(nPunti);
            points_mt[posArray[1]] = new GPointsArray(nPunti);
            points_mt[posArray[2]] = new GPointsArray(nPunti);
            points_mt[posArray[3]] = new GPointsArray(nPunti);
            points_mt[posArray[4]] = new GPointsArray(nPunti);

            // carica punti dati
            for (int i = 0; i < nPunti; i++) {
                for (int j = 0; j < 5; j++) {
                    points[posArray[j]].add(i, cSaveFileClass.getValues(posArray[j], Giro, i));
                }
            }

            // carica punti traccia madre
            if (tracciaMadre.isMasterTrackAvailiable() == true) {
                for (int i = 0; i < nPunti; i++) {
                    points_mt[posArray[0]].add(i, tracciaMadre.getSafeFrenoAnt(i));
                    points_mt[posArray[1]].add(i, tracciaMadre.getSafeFrenoPos(i));
                    points_mt[posArray[2]].add(i, tracciaMadre.getSafeAccel(i));
                    points_mt[posArray[3]].add(i, tracciaMadre.getSafeAngolo(i));
                    points_mt[posArray[4]].add(i, tracciaMadre.getSafeMarcia(i));
                }
            }

            //Dimensionamento plot area
            // part dall'alto
            altezzaRect = (displayHeight - (yUP + yDN) - dy * 4) / 5;
            larghezzaRect = (displayWidth - xz * 2);
            int y0 = yUP;
            for (int i = 0; i < 5; i++) {
                xp[i] = xz;
                yp[i] = y0;
                y0 = y0 + altezzaRect + dy;
            }
            // prepara grafica plot
            plots = new GPlot[5]; //(parent);
            GraphTitle = new String[5];
            GraphTitle[posArray[0]] = "Front brake";
            GraphTitle[posArray[1]] = "Rear brake";
            GraphTitle[posArray[2]] = "Trottle";
            GraphTitle[posArray[3]] = "Lean";
            GraphTitle[posArray[4]] = "Gear";
            plots[posArray[0]] = new GPlot(parent); //(parent);
            plots[posArray[1]] = new GPlot(parent); //(parent);
            plots[posArray[2]] = new GPlot(parent); //(parent);
            plots[posArray[3]] = new GPlot(parent); //(parent);
            plots[posArray[4]] = new GPlot(parent); //(parent);

            for (int i = 0; i < 5; i++) {
                plots[i].setPos(xp[i], yp[i]);
                plots[i].setOuterDim(larghezzaRect, altezzaRect);
                plots[i].setBgColor(0x10);
                plots[i].setBoxBgColor(0x0);
                // plots[i].setTitleText(GraphTitle[i]);
                // plots[i].getXAxis().setAxisLabelText("Time");
                // plots[i].setFontColor(myColor[i]);
                plots[i].getXAxis().setFontColor(0xffffffff);
                plots[i].getYAxis().setAxisLabelText(GraphTitle[i]);
                plots[i].getYAxis().setFontColor(myColor[i]);
                plots[i].setBoxLineColor(myColor[i]);
                plots[i].setLineColor(color(255, 255, 255));
                plots[i].setPoints(points[i]);
                // plots[i].activateZooming(2);
                plots[i].addLayer("layer MTk", points_mt[i]);
                plots[i].getLayer("layer MTk").setLineColor(color(0, 255, 255));
                if (i != 4) {
                    plots[i].getXAxis().setDrawTickLabels(false);
                }
            }

            for (int i = 0; i < 5; i++) {
                labels[i] = loadImage(MyProperties.CONF_PATH + "Grafica/plotter/" + labelTiles[i]);
            }
            String SfondoImg = MyProperties.CONF_PATH + "Grafica/plotter/SfondoGrafico.jpg";
            Sfondo = loadImage(SfondoImg);
            reportTitle = loadImage(MyProperties.CONF_PATH + "Grafica/plotter/TelemetryReport.png");

            // lap = loadImage(MyProperties.CONF_PATH + "Grafica/plotter/lap1.png");
            reportLap = new lap(Giro, nGiri, displayWidth - 250, 0, 2);

            if (nGiri > 0) {
                // int ln, int lx, int ly, int ld, String limgName, boolean lVertical
                int wb = 30 * nGiri + 40 * (nGiri - 1);
                buttons = new RadioButtonClass(nGiri, 1100, 5, 20, MyProperties.CONF_PATH + "Grafica/bottoni/radioButton", false);
                // buttons = new RadioButtonClass(nGiri, -1, displayHeight - 40, 40, MyProperties.CONF_PATH + "Grafica/bottoni/radioButton", false);
                buttons.setSelect(Giro);  // seleziona  bottone
            }
            buttonSave = new cBottone(MyProperties.CONF_PATH + "Grafica/plotter/", "buttonSaveMasterTrack", 350, 0, /*displayWidth - 350, displayHeight - 40,*/ 100);
            exitButtonPlotter = new cBottone(MyProperties.CONF_PATH + "Grafica/plotter/", "buttonExit", 25, 0, /*displayWidth - 350, displayHeight - 40,*/ 100);

            // saveMasterTrack=loadImage(MyProperties.CONF_PATH + "Grafica/plotter/saveMasterTrack.png");
            oldMouseButton = 0;
        }

        int drawGraph(int giro) {
            int retFlg;
            retFlg = -1;
            for (int c = 0; c < displayWidth; c = c + Sfondo.width) {
                for (int r = 0; r < displayHeight; r = r + Sfondo.height) {
                    image(Sfondo, c, r);
                }
            }
            image(reportTitle, (displayWidth - reportTitle.width) / 2, 0);
            for (int i = 0; i < 5; i++) {
                plots[i].beginDraw();
                plots[i].drawBackground();
                plots[i].drawBox();
                plots[i].drawXAxis();
                plots[i].drawYAxis();
                plots[i].drawTopAxis();
                plots[i].drawRightAxis();
                plots[i].drawTitle();
                // plots[i].
                // plots[i].drawPoints();
                plots[i].drawLines();
                plots[i].endDraw();
            }
            for (int i = 0; i < 5; i++) {
                image(labels[i], displayWidth - 250, yp[i] + 10, 200, 25);
            }
            // image(lap, displayWidth - 250, 20);
            reportLap.dispLap(giro);
            if (true) { // is Admin
                retFlg = buttonSave.mouse();
                if (retFlg >= 0) {
                    retFlg = retFlg | SAVE_BUTTON;
                }
            }
            if(retFlg<0) {
                retFlg = exitButtonPlotter.mouse();
                if (retFlg >= 0) {
                    retFlg = retFlg | EXIT_BUTTON;
                }
            }
            if(retFlg<0) {
                if (retFlg < 0) {
                    retFlg = buttons.mouse();
                    retFlg = retFlg | SELECT_BUTTON;
                }
            }
            if (oldMouseButton != retFlg) {
                oldMouseButton = retFlg;
            } else {
                retFlg = -1;
            }
            return retFlg;
        }

    }

    class RadioButtonClass {

        PImage imgOver;
        PImage imgSelected;
        PImage imgReleased;
        int nButtons;
        int selected;
        int oldSelected;
        int over;
        int oldOver;
        int x, y, d;
        boolean verticale;
        String imgNameOver;
        String imgNameSelected;
        String imgNameReleased;
        int[] xpos;
        int[] ypos;
        int[] xpos1;
        int[] ypos1;

        RadioButtonClass(int ln, int lx, int ly, int ld, String limgName, boolean lVertical) {

            x = lx;
            y = ly;
            d = ld;
            imgNameOver = limgName + "Over.png";
            imgNameSelected = limgName + "Selected.png";
            imgNameReleased = limgName + "Released.png";
            imgOver = loadImage(imgNameOver);
            imgSelected = loadImage(imgNameSelected);
            imgReleased = loadImage(imgNameReleased);
            verticale = lVertical;
            nButtons = ln;
            over = -1;
            xpos = new int[nButtons];
            ypos = new int[nButtons];
            xpos1 = new int[nButtons];
            ypos1 = new int[nButtons];

            if (x == -1) {
                // ricalcola x centrato
                int z = 0;
                z = z + imgReleased.width * nButtons;
                z = z + d * (nButtons - 1);
                x = (displayWidth - z) / 2;
                lx = x;
            }

            for (int i = 0; i < nButtons; i++) {
                if (verticale == true) {
                    xpos[i] = x;
                    xpos1[i] = x + imgOver.width;
                    ypos[i] = ly;
                    ypos1[i] = ly + imgOver.height;
                    ly = ly + d + imgOver.height;
                } else {
                    ypos[i] = y;
                    xpos[i] = lx;
                    ypos1[i] = y + imgOver.height;
                    xpos1[i] = lx + imgOver.width;
                    lx = lx + imgOver.width + d;
                }
            }
        }

        public void setSelect(int i) {
            selected = i;
            repaint();
        }

        void repaint() {
            if (nButtons > 1) {
                for (int i = 0; i < nButtons; i++) {
                    if (i == selected) {
                        image(imgSelected, xpos[i], ypos[i]);
                    } else {
                        if (i == over) {
                            image(imgOver, xpos[i], ypos[i]);
                        } else {
                            image(imgReleased, xpos[i], ypos[i]);
                        }
                    }
                }
            }
        }

        public int mouse() {
            int flg = -1;
            over = -1;
            if (nButtons > 1) {
                for (int i = 0; i < nButtons; i++) {
                    if (mouseX >= xpos[i] && mouseX <= xpos1[i]) {
                        if (mouseY >= ypos[i] && mouseY <= ypos1[i]) {
                            over = i;
                            if (mousePressed == true && mouseButton == LEFT) {
                                selected = i;
                                flg = i;
                                over = -1;
                            }
                        }
                    }
                }
            }
//    if(selected!=oldSelected || over!=oldOver) {
//      oldSelected=selected;
//      oldOver=over;
            repaint();
//    }
            return flg;
        }
    }

    class lap {

        int numeGiri;
        int numGiro;
        int xpos;
        int ypos;
        int inter;
        PImage imgSfondo;
        PImage imgLap;
        PImage imgSlash;
        PImage[] imgNumeri;

        lap(int lnumeGiro, int lnumeGiri, int lxpos, int lypos, int linter) {
            numeGiri = lnumeGiri;
            numGiro = lnumeGiro;
            xpos = lxpos;
            ypos = lypos;
            inter = linter;
            imgSfondo = loadImage(MyProperties.CONF_PATH + "Grafica/lap/lapSfondo.png");
            imgLap = loadImage(MyProperties.CONF_PATH + "Grafica/lap/lap.png");
            imgSlash = loadImage(MyProperties.CONF_PATH + "Grafica/lap/slash.png");
            imgNumeri = new PImage[10];
            for (int i = 0; i < 10; i++) {
                imgNumeri[i] = loadImage(MyProperties.CONF_PATH + "Grafica/lap/" + i + ".png");
            }
        }

        void dispLap(int giro) {
            int lnumeGiri = numeGiri;
            giro = giro + 1;
            int x;
            int spostVert = 8;
            x = xpos;
            image(imgSfondo, x, ypos);
            x = x + 45;
            image(imgLap, x, ypos + spostVert);
            x = x + imgLap.width + inter * 2;
            if (giro > 9) {
                image(imgNumeri[giro / 10], x, ypos + spostVert);
                x = x + imgNumeri[giro / 10].width + inter;
                giro = giro % 10;
            }
            image(imgNumeri[giro], x, ypos + spostVert);

            x = x + imgNumeri[giro].width + 1;
            image(imgSlash, x, ypos + spostVert);
            x = x + imgSlash.width + 1;

            if (lnumeGiri > 9) {
                image(imgNumeri[lnumeGiri / 10], x, ypos + spostVert);
                x = x + imgNumeri[lnumeGiri / 10].width + inter;
                lnumeGiri = lnumeGiri % 10;
            }
            image(imgNumeri[lnumeGiri], x, ypos + spostVert);
        }
    }

    public class dialogBox {

        PImage Yes;
        PImage No;
        PImage Sfondo;
        int x, y;
        int xYes, yYes;
        int xNo, yNo;

        dialogBox() {
            Yes = loadImage(MyProperties.CONF_PATH + "Grafica/dialogBox/yes.png");
            No = loadImage(MyProperties.CONF_PATH + "Grafica/dialogBox/no.png");
            Sfondo = loadImage(MyProperties.CONF_PATH + "Grafica/dialogBox/sfondo.png");
            x = (displayWidth - Sfondo.width) / 2;
            y = (displayHeight - Sfondo.height) / 2;
            xYes = x + 50;
            yYes = y + Sfondo.height - 50 - Yes.height;
            yNo = yYes;
            xNo = x + Sfondo.width - 50 - No.width;
        }

        void dispDialog() {
            image(Sfondo, x, y);
            image(Yes, xYes, yYes);
            image(No, xNo, yNo);
        }

        int testMouse() {
            int flg = -1;
            if (mouseX >= xYes && mouseX <= (xYes + Yes.width)) {
                if (mouseY >= yYes && mouseY <= (yYes + Yes.height)) {
                    if (mousePressed == true && mouseButton == LEFT) {
                        flg = BUTTON_YES;
                    }
                }
            }
            if (mouseX >= xNo && mouseX <= (xNo + No.width)) {
                if (mouseY >= yNo && mouseY <= (yNo + No.height)) {
                    if (mousePressed == true && mouseButton == LEFT) {
                        flg = BUTTON_NO;
                    }
                }
            }
            return flg;
        }

    }

    public class done {

        PImage imgDone;
        int x, y;
        int timer;
        int color;

        done() {
            timer = -1;
            color = 255;
            imgDone = loadImage(MyProperties.CONF_PATH + "Grafica/dialogBox/done.png");
            x = (displayWidth - imgDone.width) / 2;
            y = (displayHeight - imgDone.height) / 2;
        }

        int dispDone() {
            int flg;
            flg = -1;
            if (timer < 0) {
                timer = millis() + 1000;
                color = 255;
            }
            if (timer >= millis()) {

                tint(255, color);
                image(imgDone, x, y);
                if (color > 0) {
                    color--;
                }
                noTint();
            } else {
                timer = -1;
                flg = 0;
            }
            return flg;
        }
    }

    public class VersionClass {

        String ArduinoVersion;
        String ProgramVersion;
        int buffIndex=-1;
        byte[] buffVersion;
        String Rev="Rev. ";

        VersionClass() {
            ArduinoVersion = "";
            ProgramVersion = "0.0.1";
            buffVersion = new byte[20];
        }
        
        int buildVersion(byte c) 
        {
           int i;
           i=-1;
            switch(c) {
                case (byte)0xf0:
                    buffIndex=0;
                    break;
                case (byte)0xf7:
                    // finito ?
                    for(i=0;i<Rev.length();i++) {
                        if(buffVersion[i]!=Rev.charAt(i)) {
                            buffIndex=-1;
                            i=-1;
                        }
                    }
                    if(buffIndex>=0) {
                        while(buffVersion[i]!=(byte)0xf7) {
                            ArduinoVersion = ArduinoVersion + buffVersion[i];
                            i++;
                        }
                    }
                 break;
                default:
                    if(buffIndex>=0) {
                        buffVersion[buffIndex]=c;
                        buffIndex++;
                        if(buffIndex==20)
                            buffIndex=-1;
                    }
                    break;
            }
            return i;
        }

        public void setArduinoVersion(byte[] v) {
            ArduinoVersion = v.toString();
        }

        public String getArduinoVersion() {
            return ArduinoVersion;
        }

        public String getProgramVersion() {
            return ProgramVersion;
        }
    }

    /* =====================================================
 La classe gestisce la grafica dei seguenti controlli:
 - Giroscopio
 - Freno Anteriore
 - Freno Posteriore
 - Manopola del gas
 ======================================================== */
    class cAnalog {

        float angolo;
        float angolo_tm;
        int angoloxFile;
        int frenoAnt;
        int frenoAnt_tm;

        int frenoPos;
        int frenoPos_tm;
        float gas;
        float gas_tm;

        int numStepFreniAnt;
        int numStepFreniPos;

        int numStepGas;

        final static String FRENI_FILENAME = "Freni/freno_";
        final static String FRENI_TM_FILENAME = "Freni/freno_tm_";
        final static String SFONDO_1_FILENAME = "sfondo_1";
        final static String SFONDO_2_FILENAME = "sfondo_2";
        final static String SFONDO_3_FILENAME = "sfondo_3";
        final static String SFONDO_4_FILENAME = "sfondo_4";
        final static String INCL_TM_FILENAME = "inclinazione_tm";
        final static String INCL_FILENAME = "inclinazione";

        final static int ANALOG_STEPS = 100;

        final static float A2 = (2 * PI + HALF_PI / 2);
        final static float A1 = (PI - HALF_PI / 2);

        final static float GAS_LIMIT = (1);

        // 10 bitmap
        PImage img_incl_tm;  // Inclinazione traccia madre
        PImage img_incl;  // Inclinazione 
        PImage img_incl_err;  // Inclinazione 

        PImage[] freno_ant_tm;
        PImage[] freno_pos_tm;

        PImage[] freno_ant;
        PImage[] freno_pos;

        PImage[] freno_ant_err;
        PImage[] freno_pos_err;

        PImage sfondo_1;
        PImage sfondo_2;
        PImage sfondo_3;
        PImage sfondo_4;

        int xCenter;
        int yCenter;

        int analogFrenoAnt, analogFrenoAnt_tm;
        int analogFrenoPos, analogFrenoPos_tm;
        int analogAccel, analogAccel_tm;
        int analogAngolo, analogAngolo_tm;

        public int err_FrenoAnt = 0;
        public int err_FrenoPos = 0;
        public int err_Accel = 0;
        public int err_Angolo = 0;

        cAnalog(int xc, int yc, int sFreniAnt, int sFreniPos, String pathFolder) {
            String lPath;
            int i;
            xCenter = xc;
            yCenter = yc;
            numStepFreniAnt = sFreniAnt;
            numStepFreniPos = sFreniPos;
            i = pathFolder.length();
            char c = pathFolder.charAt(i - 1);
            if (c == '/' || c == '\\') {
                lPath = pathFolder.substring(0, i - 2);
            } else {
                lPath = pathFolder + "/";
            }
            // carico immagine freni

            freno_ant_tm = new PImage[numStepFreniAnt + 1];
            freno_pos_tm = new PImage[numStepFreniAnt + 1];
            freno_ant = new PImage[numStepFreniAnt + 1];
            freno_pos = new PImage[numStepFreniAnt + 1];

            freno_ant_err = new PImage[numStepFreniAnt + 1];
            freno_pos_err = new PImage[numStepFreniAnt + 1];

            for (i = 0; i <= numStepFreniAnt; i++) {
                // println(lPath+FRENI_FILENAME+i+".png");
                // println(lPath+FRENI_TM_FILENAME+i+".png");
                freno_ant[i] = loadImage(lPath + FRENI_FILENAME + i + ".png");
                freno_ant_err[i] = loadImage(lPath + FRENI_FILENAME + i + "_err.png");
                freno_ant_tm[i] = loadImage(lPath + FRENI_TM_FILENAME + i + ".png");
            }
            for (i = 0; i <= numStepFreniAnt; i++) {
                freno_pos[i] = loadImage(lPath + FRENI_FILENAME + i + ".png");
                freno_pos_err[i] = loadImage(lPath + FRENI_FILENAME + i + "_err.png");
                freno_pos_tm[i] = loadImage(lPath + FRENI_TM_FILENAME + i + ".png");
            }
            img_incl_tm = loadImage(lPath + INCL_TM_FILENAME + ".png");
            img_incl = loadImage(lPath + INCL_FILENAME + ".png");
            img_incl_err = loadImage(lPath + INCL_FILENAME + "_err.png");

            sfondo_1 = loadImage(lPath + SFONDO_1_FILENAME + ".png");
            sfondo_2 = loadImage(lPath + SFONDO_2_FILENAME + ".png");
            sfondo_3 = loadImage(lPath + SFONDO_3_FILENAME + ".png");
            sfondo_4 = loadImage(lPath + SFONDO_4_FILENAME + ".png");

            angolo = 0;
            angolo_tm = 0;
            frenoAnt = 0;
            frenoAnt_tm = 0;

            frenoPos = 0;
            frenoPos_tm = 0;
            gas = 0;
            gas_tm = 0;

            numStepGas = 0;

            setAcceleratore(1);
            setAcceleratore_tm(1);
            setInclinazione(90);
            setInclinazione_tm(0);
            setFrenoAnt(0);
            setFrenoAnt_tm(0);
            setFrenoPos(0);
            setFrenoPos_tm(0);
        }

        public void visAnalog() {
            pushMatrix();
            translate(xCenter, yCenter);
            // freni
            if (err_FrenoAnt == 0) {
                image(freno_ant[frenoAnt], sfondo_1.width / 2 - 10, -70);
            } else {
                image(freno_ant_err[frenoAnt], sfondo_1.width / 2 - 10, -70);
            }
            if(tracciaMadre.nonDisponibile==0)
                image(freno_ant_tm[frenoAnt_tm], sfondo_1.width / 2 - 10, -40);

            if (err_FrenoPos == 0) {
                image(freno_pos[frenoPos], sfondo_1.width / 2 - 10, +40 - freno_pos[0].height);
            } else {
                image(freno_pos_err[frenoPos], sfondo_1.width / 2 - 10, +40 - freno_pos[0].height);
            }
            if(tracciaMadre.nonDisponibile==0)
                image(freno_pos_tm[frenoPos_tm], sfondo_1.width / 2 - 10, +70 - freno_pos[0].height);

            imageMode(CENTER);  // Set rectMode to RADIUS
            image(sfondo_1, 0, 0);
             if(tracciaMadre.nonDisponibile==0) {
                pushMatrix();
                rotate(angolo_tm);
                image(img_incl_tm, 0, -sfondo_1.height / 2 + 20);
                popMatrix();
             }
            pushMatrix();
            rotate(angolo);
            if (err_Angolo == 0) {
                image(img_incl, 0, -sfondo_1.height / 2 + 55);
            } else {
                image(img_incl_err, 0, -sfondo_1.height / 2 + 55);
            }
            popMatrix();
            image(sfondo_2, 0, 0);
            if(tracciaMadre.nonDisponibile==0) {
                fill(255, 255, 0);
                arc(0, 0, sfondo_2.height / 2 + 100, sfondo_2.height / 2 + 100, gas_tm, PI * 2 + HALF_PI / 2);
            }
            image(sfondo_3, 0, 0);
            if (err_Accel == 0) {
                fill(0, 255, 0);
            } else {
                fill(255, 0, 0);
            }
            // arc(0, 0, sfondo_3.height/2+70, sfondo_3.height/2+70, PI-HALF_PI/2, PI*2+HALF_PI/2);
            arc(0, 0, sfondo_3.height / 2 + 70, sfondo_3.height / 2 + 70, gas, A2);
            image(sfondo_4, 0, 0);

            imageMode(CORNER);  // Set rectMode to CORNER

            popMatrix();
        }
        int currval;

        public void testincFreno() {
            currval++;
            if (currval > ANALOG_STEPS) {
                currval = ANALOG_STEPS;
            }
            setFrenoAnt(currval);
        }

        public void testdecFreno() {
            currval--;
            if (currval < 0) {
                currval = 0;
            }
            setFrenoAnt(currval);
        }

        public void setFrenoAnt(int n) // n 0 - 100
        {
            analogFrenoAnt = n;
            cErrorClass.testErrorFrenoAnt();
            frenoAnt = n * numStepFreniAnt / ANALOG_STEPS;
        }

        public void setFrenoAnt_tm(int n) // n 0 - 100
        {
            analogFrenoAnt_tm = n;
            cErrorClass.testErrorFrenoAnt();
            frenoAnt_tm = n * numStepFreniAnt / ANALOG_STEPS;
        }

        public void setFrenoPos(int n) // n 0 - 100
        {
            analogFrenoPos = n;
            cErrorClass.testErrorFrenoPos();

            frenoPos = n * numStepFreniPos / ANALOG_STEPS;
        }

        public void setFrenoPos_tm(int n) // n 0 - 100
        {
            analogFrenoPos_tm = n;
            cErrorClass.testErrorFrenoPos();
            frenoPos_tm = n * numStepFreniPos / ANALOG_STEPS;
        }

        public void setAcceleratore(int n) {
            analogAccel = n;
            cErrorClass.testErrorAccel();
            gas = -(A2 - A1) * (float) n / (float) ANALOG_STEPS + A2;
        }

        public void setAcceleratore_tm(int n) {
            analogAccel_tm = n;
            cErrorClass.testErrorAccel();
            gas_tm = -(A2 - A1) * (float) n / (float) ANALOG_STEPS + A2;
        }

        public void setInclinazione(int n) {
            // n = 0-180
            // n=n-90; // 90 = posizione centrale
            angoloxFile = n;
            analogAngolo = n; //+ 90;
            cErrorClass.testErrorAngolo();
            angolo = radians(n);
        }

        public void setInclinazione_tm(int n) {
            // n = 0-180
            // n=n-90; // 90 = posizione centrale
            // sul file sono riportati dati tra -90 <-> 0 <-> 90
            analogAngolo_tm = n; // - 90;
            cErrorClass.testErrorAngolo();
            angolo_tm = radians(n);
        }
    }

    final static String RELEASED
            = "Released";
    final static String OVER
            = "Over";
    final static String PRESSED
            = "Pressed";

    class cBottone {

        PImage released;
        PImage over;
        PImage pressed;
        int status;
        int left;
        int right;
        int top;
        int bottom;
        int x;
        int y;
        int mousIn;
        float res;
        int sx;
        int sy;

        cBottone(String path, String name, int lx, int ly, int resize) {
            released = loadImage(path + name + RELEASED + ".png");
            over = loadImage(path + name + OVER + ".png");
            pressed = loadImage(path + name + PRESSED + ".png");
            status = 0;
            x = lx;
            y = ly;
            res = (float) resize;
            sx = (int) ((float) released.width * (res / 100));
            sy = (int) ((float) released.height * (res / 100));
            right = x + sx;
            bottom = y + sy;
            top = y;
            left = x;

            mousIn = 0;
        }

        int mouse() {
            int x = testMouse();
            drawBottone();
            return x;
        }

        public void drawBottone() {
            switch (status) {
                case 0: // released
                    image(released, x, y, sx, sy);
                    break;
                case 1: // over
                    image(over, x, y, sx, sy);
                    break;
                case 2: // pressed
                    image(pressed, x, y, sx, sy);
                    break;
            }
        }

        public void drawBottone(int x, int y) {
            switch (status) {
                case 0: // released
                    image(released, x, y, sx, sy);
                    break;
                case 1: // over
                    image(over, x, y, sx, sy);
                    break;
                case 2: // pressed
                    image(pressed, x, y, sx, sy);
                    break;
            }
        }

        public int testMouse() {
            int flg;
            int ret;
            flg = 0;
            ret = -1;
            // println("X: "+mouseX+" Y: "+mouseY);
            if (mouseX >= left && mouseX <= right) {
                if (mouseY >= top && mouseY <= bottom) {
                    if (mousePressed == true && mouseButton == LEFT) {
                        flg = 2;
                        ret = 1;
                    } else {
                        flg = 1;
                    }
                }
            }
            status = flg;
            return ret;
        }
    }

    static final int ANGOLO_FINTO = 0x1000;

    class cHandleData {

        interPol cAnalog0;
        interPol cAnalog1;
        interPol cAnalog2;
        int oldSwtImg;
        int oldMarcia_tm;
        int oldAngolo_tm;
        int oldFrenoAnt_tm;
        int oldFrenoPos_tm;
        int oldAccel_tm;

        static final int MARCIA_UP = 0x01;
        static final int MARCIA_DN = 0x02;

        cHandleData() {
            cAnalog0 = new interPol(100, 900, 3, 3);
            cAnalog1 = new interPol(100, 900, 3, 3);
            cAnalog2 = new interPol(100, 900, 3, 3);
            oldSwtImg = 0;
            oldMarcia_tm = -1;
            oldAngolo_tm = 0xffff;
            oldFrenoAnt_tm = -1;
            oldFrenoPos_tm = -1;
            oldAccel_tm = -1;

        }

        void initOldDataTracciaMadre() {
            oldMarcia_tm = -1;
            oldAngolo_tm = 0xffff;
            oldFrenoAnt_tm = -1;
            oldFrenoPos_tm = -1;
            oldAccel_tm = -1;
        }

        public void aggiornaDatiTaratura() {
            int minimo, massimo;
            String sMinimo;
            String sMassimo;
            // freno ANTERIORE
            sMinimo = (String) MyProperties.PROPS.getOrDefault(MyProperties.TARATURA_ANTERIORE_MINIMO, "0");
            sMassimo = (String) MyProperties.PROPS.getOrDefault(MyProperties.TARATURA_ANTERIORE_MASSIMO, "1023");
            minimo = Integer.parseInt(sMinimo);
            massimo = Integer.parseInt(sMassimo);

            cAnalog0 = new interPol(minimo, massimo, 3, 3);

            sMinimo = (String) MyProperties.PROPS.getOrDefault(MyProperties.TARATURA_POSTERIORE_MINIMO, "0");
            sMassimo = (String) MyProperties.PROPS.getOrDefault(MyProperties.TARATURA_POSTERIORE_MASSIMO, "1023");

            minimo = Integer.parseInt(sMinimo);
            massimo = Integer.parseInt(sMassimo);

            cAnalog1 = new interPol(minimo, massimo, 3, 3);

            sMinimo = (String) MyProperties.PROPS.getOrDefault(MyProperties.TARATURA_ACCELERATORE_MINIMO, "0");
            sMassimo = (String) MyProperties.PROPS.getOrDefault(MyProperties.TARATURA_ACCELERATORE_MASSIMO, "1023");
            minimo = Integer.parseInt(sMinimo);
            massimo = Integer.parseInt(sMassimo);

            cAnalog2 = new interPol(minimo, massimo, 3, 3);

        }

        public void setAnalog(int analogNumber) {
            switch (analogNumber) {
                case 0:
                    gcAnalog.setFrenoAnt(cAnalog0.getInterpolData(vAnalog0));
                    break;
                case 1:
                    gcAnalog.setFrenoPos(cAnalog1.getInterpolData(vAnalog1));
                    break;
                case 2:
                    gcAnalog.setAcceleratore(cAnalog2.getInterpolData(vAnalog2));
                    break;
            }
        }

        public void setAngolo(int n) {
            // da seriale l'angolo va da zero a 90
            // la macchina accetta angoli da -90 a +90
            gcAnalog.setInclinazione(n - 90);
        }

        public void setSwtOnOff(int s) {
            int x;
            x = oldSwtImg ^ s;
            // println("  ->"+s+" old: "+oldSwtImg+" XOR: "+x);

            if (x != 0) {
                if ((s & MARCIA_UP) == MARCIA_UP) {
                    gcMarce.incMarcia();
                } else {
                    if ((s & MARCIA_DN) == MARCIA_DN) {
                        gcMarce.decMarcia();
                    }
                }
                oldSwtImg = s;
            }
        }

        // ----------------------- SET DATA TRACCIA MADRE
        public void updateAllData(int t) {
            set_Marcetm(t);
            set_FrenoAnttm(t);
            set_FrenoPostm(t);
            set_Acceltm(t);
            set_Angolotm(t);
        }

        public void set_Marcetm(int t) {
            t = tracciaMadre.getMarcia(t);
            if (t >= 0) { // legge dalla traccia madre. se t<0 significa che Ã¨ finita e pertanto rimane l'ultimo valore
                if (oldMarcia_tm != t) {
                    gcMarce.setMarciaRiferimento(t);
                    oldMarcia_tm = t;
                }
            }
        }

        public void set_FrenoAnttm(int t) {
            t = tracciaMadre.getFrenoAnt(t);
            if (t >= 0) { // legge dalla traccia madre. se t<0 significa che Ã¨ finita e pertanto rimane l'ultimo valore
                if (oldFrenoAnt_tm != t) {
                    gcAnalog.setFrenoAnt_tm(t);
                    oldFrenoAnt_tm = t;
                }
            }
        }

        public void set_FrenoPostm(int t) {
            t = tracciaMadre.getFrenoPos(t);
            if (t >= 0) { // legge dalla traccia madre. se t<0 significa che Ã¨ finita e pertanto rimane l'ultimo valore
                if (oldFrenoPos_tm != t) {
                    gcAnalog.setFrenoPos_tm(t);
                    oldFrenoPos_tm = t;
                }
            }
        }

        public void set_Acceltm(int t) {
            t = tracciaMadre.getAccel(t);
            if (t >= 0) { // legge dalla traccia madre. se t<0 significa che Ã¨ finita e pertanto rimane l'ultimo valore
                if (oldAccel_tm != t) {
                    gcAnalog.setAcceleratore_tm(t);
                    oldAccel_tm = t;
                }
            }
        }

        public void set_Angolotm(int t) {
            t = tracciaMadre.getAngolo(t);
            if (t != ANGOLO_FINTO) { // legge dalla traccia madre. se t<0 significa che Ã¨ finita e pertanto rimane l'ultimo valore
                if (oldAngolo_tm != t) {
                    gcAnalog.setInclinazione_tm(t);
                    oldAngolo_tm = t;
                }
            }
        }
        //boolean requireArduinoFirmwareVersion()
        //{
        //  int tentativi=10;
        //  while (tentativi>=0) {
        //    myPort.write(REQUIRE_VERSION);
        //    delay(29);  // aspetta 20 ms
        //    parseData();
        //    if (Version.getArduinoVersion()!="")
        //      return true;
        //    tentativi--;
        //  }
        //  return false;
        //}
    }

    /* la classe serve a generare i dati interpolati da 0 a 100 */
//class interPol {
//  private static final float MAXV = 100;
//  private static final int NVAL = 1024;
//  private  int[] interpolVect;
//  private int oldInputValue;
//
//  interPol(int vMin, int vMax, int dl, int dh)
//  {
//    float m;
//    float n;
//    interpolVect = new int[NVAL];
//    vMax=vMax-dh;
//    vMin=vMin+dl;
//    m=MAXV/(vMax-vMin);
//    n=-((MAXV * vMin)/(vMax-vMin));
//    int i;
//    for (i=0; i<NVAL; i++) {
//      if (i<vMin) {
//        interpolVect[i]=0;
//      } else {
//        if (i>vMax)
//          interpolVect[i]=(int)MAXV;
//        else 
//        interpolVect[i]=(int)(m*i+n+0.5f);
//      }
//    }
//  }
//
//
//  public int getInterpolData(int x)
//  {
//    if (x>=NVAL || x<0)
//      return oldInputValue;
//    oldInputValue=x;
//    return interpolVect[x];
//  }
//}
//   _______ _____            _____ _____ _____            __  __          _____  _____  ______ 
//  |__   __|  __ \     /\   / ____/ ____|_   _|   /\     |  \/  |   /\   |  __ \|  __ \|  ____|
//     | |  | |__) |   /  \ | |   | |      | |    /  \    | \  / |  /  \  | |  | | |__) | |__   
//     | |  |  _  /   / /\ \| |   | |      | |   / /\ \   | |\/| | / /\ \ | |  | |  _  /|  __|  
//     | |  | | \ \  / ____ \ |___| |____ _| |_ / ____ \  | |  | |/ ____ \| |__| | | \ \| |____ 
//     |_|  |_|  \_\/_/    \_\_____\_____|_____/_/    \_\ |_|  |_/_/    \_\_____/|_|  \_\______|
/*
la traccia madre Ã¨ composta da record i cui campi sono separati da TAB
 marce <TAB> FrenoAnt <TAB> FrenoPost <TAB> Accel <TAB> Angolo <TAB>
 
 i dati letti vengono appoggiati su array la cui lungezza Ã¨ 2400
 e che viene letto dal sistema all'incirca ogno 0,1 secondi
 2400 = 4 * 60 * 10 -> 4minuti x 60 secondi * 10
     */
    final static int MARCE_POS = 0;
    final static int FRENOANT_POS = 1;
    final static int FRENOPOS_POS = 2;
    final static int ACCEL_POS = 3;
    final static int ANGOLO_POS = 4;

    final static int NUM_RECORD = (10 * 10 * 60);

    class leggiFile {

        int[] tmMarce;
        int[] tmFrenoAnt;
        int[] tmFrenoPos;
        int[] tmAccel;
        int[] tmAngolo;
        int nonDisponibile;

        public boolean isMasterTrackAvailiable() {
            return (nonDisponibile == 0);
        }

        leggiFile() {
            String Filename = MyProperties.CONF_PATH_CIRCUITS + TRACCIA_MADRE;
            String line = null;
            int i;

            if (TRACCIA_MADRE == null) {
                nonDisponibile = 1;
                return;
            }

            tmMarce = new int[NUM_RECORD];
            tmFrenoAnt = new int[NUM_RECORD];
            tmFrenoPos = new int[NUM_RECORD];
            tmAccel = new int[NUM_RECORD];
            tmAngolo = new int[NUM_RECORD];

            BufferedReader reader = createReader(Filename);
            if (reader == null) {
                nonDisponibile = 1;
                return;
            }
            i = 0;
            nonDisponibile = 0;
            try {
                while ((line = reader.readLine()) != null) {
                    if (line.charAt(0) != '#') {
                        String[] pieces = split(line, TAB);
                        if (pieces.length == 1) {
                            pieces = split(line, ",");
                        }
                        tmMarce[i] = PApplet.parseInt(pieces[MARCE_POS]);
                        tmFrenoAnt[i] = PApplet.parseInt(pieces[FRENOANT_POS]);
                        tmFrenoPos[i] = PApplet.parseInt(pieces[FRENOPOS_POS]);
                        tmAccel[i] = PApplet.parseInt(pieces[ACCEL_POS]);
                        tmAngolo[i] = PApplet.parseInt(pieces[ANGOLO_POS]);
                        i++;
                    }
                }
                reader.close();
                for (; i < NUM_RECORD; i++) {
                    tmMarce[i] = -1;
                    tmFrenoAnt[i] = -1;
                    tmFrenoPos[i] = -1;
                    tmAccel[i] = -1;
                    tmAngolo[i] = ANGOLO_FINTO;
                }
            } catch (IOException e) {
                e.printStackTrace();
                nonDisponibile = 1;
            }
        }

        public int getMarcia(int i) {
            int l;
            l = -1;
            if (nonDisponibile == 0) {
                l = tmMarce[i];
            }
            return l;
        }

        public int getFrenoAnt(int i) {
            int l;
            l = 0;
            if (nonDisponibile == 0) {
                l = tmFrenoAnt[i];
            }
            return l;
        }

        public int getFrenoPos(int i) {
            int l;
            l = 0;
            if (nonDisponibile == 0) {
                l = tmFrenoPos[i];
            }
            return l;
        }

        public int getAngolo(int i) {
            int l;
            l = 0;
            if (nonDisponibile == 0) {
                l = tmAngolo[i];
            }
            return l;
        }

        public int getAccel(int i) {
            int l;
            l = 0;
            if (nonDisponibile == 0) {
                l = tmAccel[i];
            }
            return l;
        }

        int safeMArcia;
        int safeFrenoAnt;
        int safeFrenoPos;
        int safeAccel;
        int safeAngolo;

        public int getSafeMarcia(int i) {
            if (tmMarce[i] > 0) {
                safeMArcia = tmMarce[i];
            }
            return safeMArcia;
        }

        public int getSafeFrenoAnt(int i) {
            if (tmFrenoAnt[i] >= 0) {
                safeFrenoAnt = tmFrenoAnt[i];
            }
            return safeFrenoAnt;
        }

        public int getSafeFrenoPos(int i) {
            if (tmFrenoPos[i] >= 0) {
                safeFrenoPos = tmFrenoPos[i];
            }
            return safeFrenoPos;
        }

        public int getSafeAngolo(int i) {
            if (tmAngolo[i] >= 0) {
                safeAngolo = tmAngolo[i];
            }
            return safeAngolo;
        }

        public int getSafeAccel(int i) {
            int l;
            l = 0;
            if (tmAccel[i] >= 0) {
                safeAccel = tmAccel[i];
            }
            return safeAccel;
        }

    }
    static final int CENTERED = 0;
    static final int SCACCHI = 1;

    static final String DEFAULT_LOGO = MY_PATH + LOGO_IMG;
    
    

    class cLogo {

        PImage iLogo;
        int sx;
        int sy;
        int x;
        int y;
        int TimerLogo;

        class cxy {

            public float sizex, sizey;
        }
        cxy dSize;

        cLogo(String fileLogo) {
            iLogo = loadImage(fileLogo);
            if (iLogo == null) {
                // immagine non trovata: carica quella di defaulr
                iLogo = loadImage(fileLogo);
            }
            if (iLogo != null) {
                sx = iLogo.width;
                sy = iLogo.height;
                dSize = scale_into(displayWidth, displayHeight, iLogo.width, iLogo.height);
            }
            TimerLogo = -1;
        }

        public void drawBackgroundImage() {
        }

        public int drawLogo() {
            int flg;
            flg = 1;
            if (TimerLogo < 0) {
                TimerLogo = millis() + 2000;
            }
            if (iLogo != null) {
                int x = (displayWidth - (int) dSize.sizex) / 2;
                int y = (displayHeight - (int) dSize.sizey) / 2;
                //  iLogo.resize(displayWidth, displayHeight);
                //  background(iLogo);
                // image(iLogo, x, y, (int) dSize.sizex, (int) dSize.sizey);
                if (millis() > TimerLogo) {
                    flg = 0;
                }
            }
            return flg;
        }

        public cxy scale_into(int target_width, int target_height, int source_width, int source_height) {
            int p = 100;
            cxy sxy;
            sxy = new cxy();
            sxy.sizex = source_width;
            sxy.sizey = source_height;
            while (sxy.sizex > target_width || sxy.sizey > target_height) {
                p--;
                sxy.sizex = (float) (p * source_width) / 100;
                sxy.sizey = (float) (p * source_height) / 100;
            }
            return sxy;
        }
    }
    
    class myFont {

        PImage[] myFontArray;
        

        myFont(String Font, String PathName) {
            myFontArray = new PImage[95];
            int i;
            for (i = 0; i < 95; i++) {
                // String fileChar=PathName+"Tbdltaa.00000000"+String.format("%02d",i)+".png";
                String fileChar = PathName + Font + String.format("%02d", i) + ".png";
                myFontArray[i] = loadImage(fileChar);
            }
        }

        PImage getCharImage(char c) {
            int i;
            i = (int) c;
            i = i - 32;
            if (i < 0 || i > 94) {
                i = 0;
            }
            return myFontArray[i];
        }
    }

    class InfoClass {
        PFont font32;
        PFont font48;
        String Name;
        String Moto;
        String Citcuito;
        PImage Intestazione;
        myFont cmyFont;
	myFont TimerFont;
        int[] imgIndex;
        lap infoLap;
        String Time;
        PImage imgSfondo;
        float oldTimer;
        int oldC;
        
        PGraphics pName;
        
        InfoClass(String lName,String lMoto,String lCircuito) {
            cmyFont = new myFont("Tbdltaa.00000000",MyProperties.CONF_PATH+"Font/font48/");
            TimerFont = new myFont("T36.00000000",MyProperties.CONF_PATH+"Font/r36/");
            Name=lName;
            Moto=lMoto;
         
            Intestazione=loadImage(MyProperties.CONF_PATH+"Grafica/Intestazione.png");
            
            infoLap = new lap(1,numeroGiri,displayWidth - 250,100,2);
           imgSfondo = loadImage(MyProperties.CONF_PATH + "Grafica/lap/lapSfondo.png");
           oldTimer=0;

        }
        
        void dispLap(int giro)
        {   
            if(giro<numeroGiri)
                oldC=giro;
            infoLap.dispLap(oldC);
        }
        
        void dispTimer(float t,int c)
        {
            t=t/1000;
            image(imgSfondo,displayWidth - 250,150);
            if(STopTimer==0) {
                oldTimer=t;
            }
            Time=String.format("%.2f",oldTimer);
            int i;
            int sizex=displayWidth - 150;
            PImage myImg;
            for(i=0;i<Time.length();i++) {
                myImg=TimerFont.getCharImage(Time.charAt(i));
                image(myImg,sizex,150);
                sizex=sizex+myImg.width;
            }            //           
        }
        
        void dispIntestazione()
        {
            image(Intestazione,200,100);
            image(Intestazione,200,160);
            int i;
            int sizex=250;
            PImage myImg;
            for(i=0;i<Name.length();i++) {
                myImg=cmyFont.getCharImage(Name.charAt(i));
                image(myImg,sizex,100);
                sizex=sizex+myImg.width;
            }            // 
            sizex=250;
             for(i=0;i<Moto.length();i++) {
                myImg=cmyFont.getCharImage(Moto.charAt(i));
                image(myImg,sizex,160);
                sizex=sizex+myImg.width;
            }
             
        }
        
    }

    /* ----------------------  //<>// //<>// //<>//
 gestione visualizzazione marce
     */
// displayHeight
    class cMarce {

        int[] coordXMarcia;
        int[] coordYMarcia;

        int[] sizeXMarcia;
        int[] sizeYMarcia;

        int[] fatXCorrez;
        int[] fatYCorrez;

        int[] sizeXCerchioRosso;
        int[] sizeYCerchioRosso;

        int[] fatXCorrezCerchioRosso;
        int[] fatYCorrezCerchioRosso;
        int vcCerchioRosso;

        int[] tinta;

        PImage[] imgMarce;
        PImage cerchioRosso;
        PImage cerchioRosso1;
        int[] vcMarce;
        int timerRif;
        int versoMarciaRif;
        int nextRifMarcia;

        int rMarcia;
        int posxMarcia;
        int posyMarcia;
        int numMarce;
        int nMarceVisibili;
        int posCentrale;
        int nStep;
        int mTimer;

        int currMarcia;
        int nextMarcia;
        int versoMarcia = 0;

        int currRifMarcia;

        int timerError;
        int skill;  // livello di difficoltÃ : 0,1,2 -> facile, medio, difficile
        int[] timerDifficolta;

        private static final int COORINATA_FINTA = 0xffffff;
        private static final String NOME_IMG_RIF = "rif.png";
        private static final String NOME_IMG_RIF1 = "rif_er.png";
        private static final int LIVELLI_DIFFICOLTA = 3;

        cMarce(int numStepMarcia, int raggio, int x, int y, int nMarce, String imgMarcePATH, int nMarceVis, int lnStep) {
            // numStepMarcia = numero di step che l'immagine deve compiere per fare un angolo di 90 gradi
            // raggio = raggio della semicirconferenza sulla quale si muovono le marce
            // x = coordinata x del centro della semicirconferenza
            // y = coordinata y del centro della semicirconferenza
            // nMarce = numero totale delle marce (in genere 6)
            // imgMarcePATH = path name della cartella dove risiedono le immagini delle marce
            // nMarceVis = mu,erp marce visibili
            // nStep = numero di step di visualizzazione tra una marcia e la successiva sulla semocirconferenza

            int i;
            float angolo = 0;
            rMarcia = raggio;
            posxMarcia = x;
            posyMarcia = y;
            numMarce = nMarce;
            nMarceVisibili = nMarceVis;
            mTimer = -1;
            timerRif = -1;
            timerError = -1;
            versoMarciaRif = 0;
            nStep = lnStep;

            // imgMarcePATH path alla cartella immagini marce.
            // ogni immagime marcia Ã¨ del tipo 1.png, 2.png ,....
            // allocal array immagini marce
            String lPath, dlPath;
            // carica le immagini delle marce
            imgMarce = new PImage[numMarce];

            i = imgMarcePATH.length();
            char c = imgMarcePATH.charAt(i - 1);
            if (c == '/' || c == '\\') {
                dlPath = imgMarcePATH.substring(0, i - 2);
            } else {
                dlPath = imgMarcePATH + "/";
            }
            for (i = 0; i < numMarce; i++) {
                lPath = dlPath + (i + 1) + ".png";
                imgMarce[i] = loadImage(lPath);
            }

            cerchioRosso = loadImage(dlPath + NOME_IMG_RIF);
            cerchioRosso1 = loadImage(dlPath + NOME_IMG_RIF1);

            coordXMarcia = new int[numStepMarcia * 2 + 1 + numStepMarcia * numMarce * 2];
            coordYMarcia = new int[numStepMarcia * 2 + 1 + numStepMarcia * numMarce * 2];
            tinta = new int[numStepMarcia * 2 + 1 + numStepMarcia * numMarce * 2];
            sizeXMarcia = new int[numStepMarcia * 2 + 1 + numStepMarcia * numMarce * 2];
            sizeYMarcia = new int[numStepMarcia * 2 + 1 + numStepMarcia * numMarce * 2];
            fatXCorrez = new int[numStepMarcia * 2 + 1 + numStepMarcia * numMarce * 2];
            fatYCorrez = new int[numStepMarcia * 2 + 1 + numStepMarcia * numMarce * 2];

            fatXCorrezCerchioRosso = new int[numStepMarcia * 2 + 1 + numStepMarcia * numMarce * 2];
            fatYCorrezCerchioRosso = new int[numStepMarcia * 2 + 1 + numStepMarcia * numMarce * 2];
            sizeXCerchioRosso = new int[numStepMarcia * 2 + 1 + numStepMarcia * numMarce * 2];
            sizeYCerchioRosso = new int[numStepMarcia * 2 + 1 + numStepMarcia * numMarce * 2];

            int lastxd, lastxs, lasty;
            float linea;
            lastxd = COORINATA_FINTA;
            lasty = COORINATA_FINTA;
            linea = 0;
            angolo = 0;
            // calcola coordinate posizioni
            for (i = numStepMarcia * numMarce; i < numStepMarcia * 2 + 1 + numStepMarcia * numMarce; i++) {
                coordXMarcia[i] = (int) ((float) rMarcia * cos(angolo) + 0.5f) + posxMarcia;
                coordYMarcia[i] = posyMarcia - (int) ((float) rMarcia * sin(angolo) + 0.5f) - imgMarce[0].height / 2;
                angolo = angolo + (PI / (numStepMarcia * 2));
                if (lastxd == COORINATA_FINTA) {
                    lastxd = coordXMarcia[i];
                    lasty = coordYMarcia[i];
                    linea = (float) rMarcia * angolo;
                }
            }

            lastxs = coordXMarcia[i - 1];
            // ora setta le coordinate del tracciato verticale parte sinistra
            int ystep;
            ystep = 1;
            for (; i < numStepMarcia * 2 + 1 + numStepMarcia * numMarce * 2; i++) {
                coordXMarcia[i] = lastxs;
                coordYMarcia[i] = lasty + (int) ((float) linea * (float) ystep + 0.5f);
                ystep++;
            }
            // ora setta le coordinate del tracciato verticale parte destra

            ystep = 1;
            for (i = numStepMarcia * numMarce - 1; i >= 0; i--) {
                coordXMarcia[i] = lastxd;
                coordYMarcia[i] = lasty + (int) ((float) linea * (float) ystep + 0.5f);
                ystep++;
            }
            posCentrale = numStepMarcia + numStepMarcia * numMarce;
            // ricalcolo size e trasparenza
            int stepTrasparenza = 20;
            int initSize = 100;
            int stepSize = 10;
            int k = 255 - stepTrasparenza;
            int j = posCentrale + 1;
            tinta[posCentrale] = 255;

            sizeXMarcia[posCentrale] = imgMarce[0].width;
            sizeYMarcia[posCentrale] = imgMarce[0].height;
            fatXCorrez[posCentrale] = imgMarce[0].width / 2;
            fatYCorrez[posCentrale] = imgMarce[0].height / 2;

            sizeXCerchioRosso[posCentrale] = cerchioRosso.width;
            sizeYCerchioRosso[posCentrale] = cerchioRosso.height;
            fatXCorrezCerchioRosso[posCentrale] = cerchioRosso.width / 2;
            fatYCorrezCerchioRosso[posCentrale] = cerchioRosso.height / 2;

            for (i = posCentrale - 1; i >= 0; i--) {
                tinta[i] = k;
                tinta[j] = k;

                sizeXMarcia[i] = sizeXMarcia[posCentrale] * initSize / 100;
                sizeYMarcia[i] = sizeYMarcia[posCentrale] * initSize / 100;
                sizeXMarcia[j] = sizeXMarcia[posCentrale] * initSize / 100;
                sizeYMarcia[j] = sizeYMarcia[posCentrale] * initSize / 100;

                sizeXCerchioRosso[i] = sizeXCerchioRosso[posCentrale] * initSize / 100;
                sizeYCerchioRosso[i] = sizeYCerchioRosso[posCentrale] * initSize / 100;
                sizeXCerchioRosso[j] = sizeXCerchioRosso[posCentrale] * initSize / 100;
                sizeYCerchioRosso[j] = sizeYCerchioRosso[posCentrale] * initSize / 100;

                fatXCorrez[i] = sizeXMarcia[i] / 2;
                fatYCorrez[i] = sizeYMarcia[i] / 2;
                fatXCorrez[j] = sizeXMarcia[j] / 2;
                fatYCorrez[j] = sizeYMarcia[j] / 2;

                fatXCorrezCerchioRosso[i] = sizeXCerchioRosso[i] / 2;
                fatYCorrezCerchioRosso[i] = sizeYCerchioRosso[i] / 2;
                fatXCorrezCerchioRosso[j] = sizeXCerchioRosso[j] / 2;
                fatYCorrezCerchioRosso[j] = sizeYCerchioRosso[j] / 2;

                initSize = initSize - stepSize;
                if (initSize < 0) {
                    initSize = 0;
                }
                k = k - stepTrasparenza;
                if (k < 0) {
                    k = 0;
                }
                j++;
            }

            // setta il vettore immagini
            vcMarce = new int[numMarce];   // contiene il vettore che punta alla coordinata xy di visualizzazione

            timerDifficolta = new int[LIVELLI_DIFFICOLTA];
            timerDifficolta[0] = 100;
            timerDifficolta[1] = 200;
            timerDifficolta[2] = 300;
        }

        public void setMarcia(int nMarcia) // settta senza animazione la marcia
        {
            int lnStep;
            int i, j;
            lnStep = nStep;
            nMarcia--;
            currMarcia = nMarcia;
            nextMarcia = currMarcia;
            // il vettore vMarce contiene le posizioni delle marce da visualizzare
            vcMarce[nMarcia] = posCentrale; // quasto significa che la marcia nMarcia sarÃ  in posizione centrale

            // aggiusto le marce precedenti
            mTimer = -1; // blocca l'animazione
            // calcolo posizione numero marce a destra)
            i = nMarcia + 1;
            j = 1;
            while (i < numMarce) {
                vcMarce[i] = posCentrale - lnStep * j;
                j++;
                i++;
            }
            i = nMarcia - 1;
            j = 1;
            while (i >= 0) {
                vcMarce[i] = posCentrale + lnStep * j;
                j++;
                i--;
            }
            // for (i=0; i<numMarce; i++) {
            //  println("coord " + vcMarce[i]+" X="+coordXMarcia[vcMarce[i]]+" Y="+coordYMarcia[vcMarce[i]]);
            //  println("num img " +(i+1));
            // }
        }

        public void initMarciaRiferimanto(int nMarcia) {
            int p;
            nMarcia--;
            currRifMarcia = nMarcia;
            p = vcMarce[nMarcia];
            //tint(255, tinta[p]);
            //image(cerchioRosso, coordXMarcia[p]-fatXCorrezCerchioRosso[p], coordYMarcia[p]-fatYCorrezCerchioRosso[p], sizeXCerchioRosso[p], sizeYCerchioRosso[p]);
            vcCerchioRosso = p;
        }

        public void visMarciaRiferimento() {
            int p;
            p = vcCerchioRosso;
            tint(255, tinta[p]);
            if (p == posCentrale) {
                image(cerchioRosso, coordXMarcia[p] - fatXCorrezCerchioRosso[p], coordYMarcia[p] - fatYCorrezCerchioRosso[p], sizeXCerchioRosso[p], sizeYCerchioRosso[p]);
            } else {
                image(cerchioRosso1, coordXMarcia[p] - fatXCorrezCerchioRosso[p], coordYMarcia[p] - fatYCorrezCerchioRosso[p], sizeXCerchioRosso[p], sizeYCerchioRosso[p]);
            }
        }

        public void setMarciaRiferimento(int nMarcia) {
            nMarcia--;
            versoMarciaRif = vcMarce[nMarcia] - vcCerchioRosso;
            nextRifMarcia = nMarcia;
            timerRif = 2;
            TestSound();
        }

        public void visMarce() // visualizzazione marce
        {
            int i;
            int p;
            for (i = 0; i < numMarce; i++) {
                p = vcMarce[i];
                tint(255, tinta[p]);
                image(imgMarce[i], coordXMarcia[p] - fatXCorrez[p], coordYMarcia[p] - fatYCorrez[p], sizeXMarcia[p], sizeYMarcia[p]);
                // println("coord " + p +" X="+coordXMarcia[p]+" Y="+coordYMarcia[p]+" img:" + (i+1));
            }
            //image(i
            noTint();
            //mgMarce[2], 1200,1080-90);
            //image(imgMarce[1], 800,320);
            //image(imgMarce[2], 500,620);
        }

        public void incMarcia() {
            nextMarcia++;
            if (nextMarcia >= numMarce) {
                nextMarcia = numMarce - 1;
            } else {
                mTimer = 2;
                versoMarcia = 1;
                TestSound();
            }
        }

        public void decMarcia() {
            nextMarcia--;
            if (nextMarcia < 0) {
                nextMarcia = 0;
            } else {
                mTimer = 2;
                versoMarcia = -1;
                TestSound();
            }
        }

        public void TestSound() {
            if (nextRifMarcia != nextMarcia && tracciaMadre.nonDisponibile == 0) {
                cErrorClass.setErroreMarce();
            } else {
                cErrorClass.stopErroreMarce();
            }
        }

        public void timerMarcia() {
            int i;
            int p;
            if (mTimer >= 0) {
                mTimer--;
                if (mTimer == 0) {
                    if (versoMarcia > 0) { // la marcia innesttata Ã¨ > di quella corrente
                        // ruota le marce verso sx
                        for (i = 0; i < numMarce; i++) {
                            vcMarce[i]++;
                        }
                        vcCerchioRosso++;
                    } else {
                        if (versoMarcia < 0) { // la marcia innesttata Ã¨ > di quella corrente
                            // ruota le marce verso sx
                            for (i = 0; i < numMarce; i++) {
                                vcMarce[i]--;
                            }
                            vcCerchioRosso--;
                        }
                    }
                    p = vcMarce[nextMarcia];
                    if (p == posCentrale) {
                        currMarcia = nextMarcia;
                        mTimer = -1;
                        versoMarcia = 0;
                        cErrorClass.stopErroreMarce();
                    } else {
                        mTimer = 2;
                    }
                }
            }
            if (timerRif >= 0) {
                timerRif--;
                if (timerRif == 0) {
                    if (versoMarciaRif > 0) {
                        vcCerchioRosso++;
                    } else {
                        if (versoMarciaRif < 0) {
                            vcCerchioRosso--;
                        }
                    }
                    if (vcCerchioRosso == vcMarce[nextRifMarcia]) {
                        timerRif = -1;
                        versoMarciaRif = 0;
                        currRifMarcia = nextRifMarcia;
                    } else {
                        timerRif = 2;
                    }
                }
            }
        }

        public void setSkill(int lskill) {
            skill = lskill;
        }
    }

//<>// //<>// //<>// //<>// //<>//
    static final int REQUIRE_VERSION = (byte) 0xf1;
    static final byte DATI_SENSORI = 0;
    static final byte VERSION = 1;

    Serial myPort = null;
    final int fifoSize = 1024;
    final int rxBufferSize = 11;
    byte[] InputFifo = new byte[fifoSize];
    byte[] inBuffer = new byte[rxBufferSize];
    int indexR;
    int indexW;

    byte[] dataSerial = new byte[rxBufferSize];
    int dataSerialIndex;

    int vAnalog0;
    int vAnalog1;
    int vAnalog2;

    int SwtOnOff;
    int Angolo;

    int ArduinoFound;

    public void closeSerial() {
        if (myPort != null) {
            myPort.stop();
        }
        myPort = null;
    }

    public void openSerial(int i) {
        // The serial port:
        int j;
        for (j = 0; j < fifoSize; j++) {
            InputFifo[j] = 0;
        }
        indexR = 0;
        indexW = 1;
        dataSerialIndex = 0;
        dataSerial[0] = 0;
        // List all the available serial ports:
        printArray(Serial.list());
        i = 0;
        ArduinoFound = 0;
        while (i < Serial.list().length) {
            try {

                // Open the port you are using at the rate you want:
                myPort = new Serial(this, Serial.list()[i], 9600);
                //delay(100);
                //if (gcHandleData.requireArduinoFirmwareVersion()==true)
                break;

            } catch (java.lang.RuntimeException e) {
                i++;
            }
            // Send a capital A out the serial port:
            // myPort.write("M");
        }
    }

    public class MyObs extends Observable {

        public int counterBuff = 0;
        boolean toNotify = false;

        public void haveToNotify() {
            if (toNotify) {
                notifica();
            }
            toNotify = false;
        }
        byte[] bufferMariano = new byte[20];

        public MyObs() {
            addObserver(Mototrainer.motocicletta);
//            addObserver(Motocicletta.wbNew);
        }

        public byte getByteMariano(int ndx) {
            return bufferMariano[ndx];
        }

        public void setByteMariano(byte byteArduino, int ndx) {
            this.bufferMariano[ndx] = byteArduino;
        }

        public void notifica() {
            setChanged();
            notifyObservers(bufferMariano);
        }

    }
    static int contatore = 0;

    public void serialEvent(Serial myPort) { // starts reading data from the Serial Port
        //  // reads the data from the Serial Port up to the character '.' and puts it into the String variable "data".

//  System.out.println("Contatore:"+contatore);
        int i;
        while (myPort.available() > 0) {
            int n = myPort.readBytes(inBuffer);
            if (inBuffer != null) {
                for (i = 0; i < n; i++) {
                    pushFifo(inBuffer[i]);
                    if (inBuffer[i] == (byte) 0xF0) {
                        myObs.counterBuff = 0;
                        continue;
                    }
                    if (inBuffer[i] == (byte) 0xF7) {
                        myObs.toNotify = true;
                        continue;
                    }
                    myObs.setByteMariano((byte) inBuffer[i], myObs.counterBuff++);

                }

            }
        }
    }

    public void pushFifo(byte Data) {
        InputFifo[indexW] = Data;
        indexW++;
        if (indexW == fifoSize) {
            indexW = 0;
        }
    }
    
   

    public void parseData() {
        int flg;
        byte c;
        flg = 1;
        while (flg == 1) {
            c = popFifo();
            if (c != (byte) 0xff) {
                dumpData(c);
                switch (c) {
                    case (byte) 0xf0:
                        dataSerial[0] = (byte) 0xf0;
                        dataSerialIndex = 1;
                        break;

                    case (byte) 0xf7:
                        if (dataSerial[0] == (byte) 0xf0 && dataSerialIndex > 0) {
                            dataSerial[dataSerialIndex] = (byte) 0xf7;
                            dataSerialIndex = -1;
                            vAnalog0 = convert728(dataSerial[1], dataSerial[2]);
                            vAnalog1 = convert728(dataSerial[3], dataSerial[4]);
                            vAnalog2 = convert728(dataSerial[5], dataSerial[6]);
                            Angolo = convert728(dataSerial[7], dataSerial[8]);
                            Angolo = 180 - Angolo;
// print("- Angolo: "+Angolo+" Swt: "+SwtOnOff);

                            SwtOnOff = (int) dataSerial[9];
                            gcHandleData.setAnalog(0);
                            gcHandleData.setAnalog(1);
                            gcHandleData.setAnalog(2);
                            gcHandleData.setAngolo(Angolo);
                            gcHandleData.setSwtOnOff(SwtOnOff);
                        }
                        break;
                    default:
                        if ((c & (byte) 0x80) != 0) {
                            // ricevuto dato errato a 8 bit
                            dataSerial[0] = 0;  // reset data
                        }
                        if (dataSerialIndex >= rxBufferSize) {
                            println("error " + dataSerialIndex);
                            dataSerial[0] = 0;  // reset data
                        }
                        if (dataSerial[0] == (byte) 0xf0 && dataSerialIndex > 0) {
                            dataSerial[dataSerialIndex] = c;
                            dataSerialIndex++;
                        }
                        break;
                }
            } else {
                flg = 0;

            }
        }
    }

    public byte popFifo() {
        int rTmp;

        byte c;

        c = (byte) 0xff;
        rTmp = indexR + 1;

        if (rTmp == fifoSize) {
            rTmp = 0;

        }
        if (rTmp != indexW) { // indici differenti (ci sono dati)
            c = InputFifo[rTmp];
            indexR = rTmp;
        }
        return c;
    }

    public int convert728(byte h, byte l) {
        int z;

        z = h;

        z = z << 7;
        z = z | l;

        if (z < 0) {
            z = 0;
        }
        if (z > 1023) {
            z = 1023;
        }
        return z;

    }

    public void dumpData(byte c) {
//        print(hex(c, 2));
//
//        if (c == (byte) 0xf7) {
//            println(" ");
//        }
        // print(char(c));
    }

    public void settings() {
        fullScreen(P2D, 2);
    }

    static public void main(String[] passedArgs) {
        String[] appletArgs = new String[]{"TestVideoProcessing"};
        if (passedArgs != null) {
            PApplet.main(concat(appletArgs, passedArgs));
        } else {
            PApplet.main(appletArgs);
        }
    }
}
