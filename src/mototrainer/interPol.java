/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mototrainer;

/**
 *
 * @author maria
 */
public class interPol {
  private static final float MAXV = 100;
  private static final int NVAL = 1024;
  private  int[] interpolVect;
  private int oldInputValue;

//  interPol(int min, int max){
//      super(min,max,0,0);
//  }
  
  public interPol(int vMin, int vMax, int dl, int dh)
  {
    float m;
    float n;
    interpolVect = new int[NVAL];
    vMax=vMax-dh;
    vMin=vMin+dl;
    m=MAXV/(vMax-vMin);
    n=-((MAXV * vMin)/(vMax-vMin));
    int i;
    for (i=0; i<NVAL; i++) {
      if (i<vMin) {
        interpolVect[i]=0;
      } else {
        if (i>vMax)
          interpolVect[i]=(int)MAXV;
        else 
        interpolVect[i]=(int)(m*i+n+0.5);
      }
    }
  }


  public int getInterpolData(int x)
  {
    if (x>=NVAL || x<0)
      return oldInputValue;
    oldInputValue=x;
    return interpolVect[x];
  }
}

