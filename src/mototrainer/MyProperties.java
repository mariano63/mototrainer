
package mototrainer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author maria
 */
public class MyProperties extends Properties {

    //Questi 3 non sono più usati
    final static String TARATURA_CENTRALE = "TaraturaCentrale";
    final static String TARATURA_SINISTRA = "TaraturaSinistra";
    final static String TARATURA_DESTRA = "TaraturaDestra";
    
    final static String TARATURA_CENTRO = "TaraturaCentro";
    public final static String TARATURA_ACCELERATORE_MINIMO = "TaraturaAcceleratoreMinimo";
    public final static String TARATURA_ACCELERATORE_MASSIMO = "TaraturaAcceleratoreMassimo";
    public final static String TARATURA_ANTERIORE_MINIMO = "TaraturaAnterioreMinimo";
    public final static String TARATURA_ANTERIORE_MASSIMO = "TaraturaAnterioreMassimo";
    public final static String TARATURA_POSTERIORE_MINIMO = "TaraturaPosterioreMinimo";
    public final static String TARATURA_POSTERIORE_MASSIMO = "TaraturaPosterioreMassimo";
    final static String PAG1_PILOTA = "Pilota";
    final static String PAG1_MOTOCICLETTA = "Motocicletta";
    final static String PAG1_DIFFICOLTA = "Difficoltà";
    final static String PAG1_NUMERO_GIRI = "Giri";
    final static String PAG1_CIRCUITO = "Circuito";
    final static String PAG1_TELEMETRIA = "Telemetria";

    final static String DIRECTORY_CIRCUITO="DirectoryCircuito";
    final static String FILE_CIRCUITO="FileCircuito";
    final static String DIRECTORY_TELEMETRIE="DirectoryTelemetrie";
    final static String FILE_TELEMETRIE="FileTelemetrie";
    
    final static String LOOK_AND_FEEL="LookAndFeel";

    public static final String CONF_PATH = 
            System.getProperty("user.home") 
            + System.getProperty("file.separator") 
            + ".mototrainer" 
            + System.getProperty("file.separator");
    
    final static public String CONF_FILE
            = CONF_PATH
            + "dati.conf";

    final static public String CONF_PATH_CIRCUITS
            = CONF_PATH
            + "circuiti"
            + System.getProperty("file.separator");
    
    final static public String MASTER_TRACK_EXT=".mtk";
    
    public static final MyProperties PROPS = new MyProperties();

    
    public MyProperties() {
        super();
        loadConfFromXML();
    }

    
    public final void loadConfFromXML() {
        try {
            File f = new File(CONF_FILE);
            f.getParentFile().mkdirs();
            f.createNewFile();
            loadFromXML(new FileInputStream(f));
        } catch (IOException ex) {
            Logger.getLogger(MyProperties.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public final void storeConfToXML()  {
        FileOutputStream fo = null;
        try {
            File f = new File(CONF_FILE);
            fo = new FileOutputStream(f);
            storeToXML(fo, "Configurazione");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MyProperties.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MyProperties.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if(fo != null)
                    fo.close();
            } catch (IOException ex) {
                Logger.getLogger(MyProperties.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
