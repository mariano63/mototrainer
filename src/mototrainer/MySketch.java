package mototrainer;

import processing.core.*;

import processing.video.*;

/**
 *
 * @author maria
 */
public class MySketch extends PApplet {
    //instanza classe Giggio
    
    String PATHVideo = MyProperties.CONF_PATH+"video.mp4";
    String PATHimg = MyProperties.CONF_PATH+"video.png";
    Movie theMov;
    PImage img;

    int x;
    int imgx, imgy;
    int incx, incy;

    @Override
    public void setup() {

        //size(displayWidth, displayHeight,P3D);
        theMov = new Movie(this, PATHVideo);
        img = loadImage(PATHimg);
        x = 0;
        incx = 1;        incy = 1;        imgx = 0;        imgy = 0;
        /* only use 1 of the following options */
        theMov.play();  //plays the movie once
        //theMov.loop();  //plays the movie over and over

    }

    @Override
    public void draw() {
//        rect(mouseX, mouseY, 20, 20);
        image(theMov, 0, 0);
        textSize(60);
        text("Numero frame: " + x, 100, 100);
        text("Frame rate: " + frameCount, 100, 200);
        image(img, imgx, imgy);
        imgx = imgx + incx;
        imgy = imgy + incy;
        if (imgx == 0 || imgx > (displayWidth - 825)) {
            incx = -incx;
        }
        if (imgy == 0 || imgx > (displayHeight - 464)) {
            incy = -incy;
        }
        x = x + 1;
    }
    
    
public void movieEvent(Movie m) { 
  m.read(); 
} 
    @Override
  public void settings() {  fullScreen(0); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "TestVideoProcessing" };
    if (passedArgs != null) {
      MySketch.main(concat(appletArgs, passedArgs));
    } else {
      MySketch.main(appletArgs);
    }
  }
    
    
}
