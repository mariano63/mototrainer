/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mototrainer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author maria
 *
11 byte:
F0 -> Inizio messaggio

   MSB       LSB
0000_0xxx 0xxx_xxxx  2 byte (utilizzati solo 10 bit) Freno Anteriore ( 0 - 1023 ) 
0000_0yyy 0yyy_yyyy  2 byte (utilizzati solo 10 bit) Freno Posteriore ( 0 - 1023 )
0000_0zzz 0zzz_zzzz  2 byte (utilizzati solo 10 bit) Acceleratore ( 0 - 1023 )
0000_0aaa 0aaa_aaaa  2 byte (utilizzati solo 10 bit) angolo ( 0 - 1023 )
0000_00id            1 byte (utilizzati solo 2 bit)  incrementa marcia (0000_0010), decrementa marcia (0000_0001)
F7 -> Fine messaggio
 

*
 */
public class DatiArduino {

    long frame = 0;
    int acceleratore = 50;           //
    int frenoAnteriore = 0;         //Front Brake
    int frenoPosteriore = 1024;     //Rear Break
    int angolo = 90;
    boolean incrementaMarcia = false;
    boolean decrementaMarcia = false;
    int numeroMarcia = 0;
//
//            jLabelThrottleClose.setText((String) MyProperties.PROPS.getOrDefault(MyProperties.TARATURA_ACCELERATORE_MINIMO, "1024"));
//        jLabelThrottleOpen.setText((String) MyProperties.PROPS.getOrDefault(MyProperties.TARATURA_ACCELERATORE_MASSIMO, "0"));
//        jLabelInclinationMiddle.setText((String) MyProperties.PROPS.getOrDefault(MyProperties.TARATURA_CENTRO, "0"));
//        jLabelFrontBrakeAllClose.setText((String) MyProperties.PROPS.getOrDefault(MyProperties.TARATURA_ANTERIORE_MINIMO, "1024"));
//        jLabelFrontBrakeAllOpen.setText((String) MyProperties.PROPS.getOrDefault(MyProperties.TARATURA_ANTERIORE_MASSIMO, "0"));
//        jLabelRearBrakeAllClose.setText((String) MyProperties.PROPS.getOrDefault(MyProperties.TARATURA_POSTERIORE_MINIMO, "1024"));
//        jLabelRearBrakeAllOpen.setText((String) MyProperties.PROPS.getOrDefault(MyProperties.TARATURA_POSTERIORE_MASSIMO, "0"));

    final synchronized public void creaStruttura(byte[] buffer, long frame, MyProperties myProp) {
        this.frame = frame;
        creaStruttura(buffer, myProp);
    }

    private synchronized int manageMinMax(int bufferPos, byte[] buffer, MyProperties myProp) {
        String keyMin, keyMax;
        switch (bufferPos) {
            case 4: 
                keyMin = MyProperties.TARATURA_ACCELERATORE_MINIMO;
                keyMax = MyProperties.TARATURA_ACCELERATORE_MASSIMO;
                break;
            case 0:
                keyMin = MyProperties.TARATURA_ANTERIORE_MINIMO;
                keyMax = MyProperties.TARATURA_ANTERIORE_MASSIMO;
                break;
            case 2:
            default:
                keyMin = MyProperties.TARATURA_POSTERIORE_MINIMO;
                keyMax = MyProperties.TARATURA_POSTERIORE_MASSIMO;
        }

        int max = Integer.parseInt((String) myProp.getOrDefault(keyMax, "0"));
        int min = Integer.parseInt((String) myProp.getOrDefault(keyMin, "1024"));
        int value = Utility.from10BitsToInt(buffer[bufferPos], buffer[bufferPos + 1]);
        if (value < min) {
            min = value;
            myProp.setProperty(keyMin, value + "");
        }
        if (value > max) {
            max = value;
            myProp.setProperty(keyMax, value + "");
        }
        interPol iPol = new interPol(min, max, 3, 3);
        return iPol.getInterpolData(value);
    }

    final synchronized public void creaStruttura(byte[] buffer, MyProperties myProp) {
        //acceleratore
        this.acceleratore = manageMinMax(4, buffer, myProp);

        //frenoAnteriore
        this.frenoAnteriore = manageMinMax(0, buffer, myProp);

        //frenoPosteriore
        this.frenoPosteriore = manageMinMax(2, buffer, myProp);
        //angolo
        int centro = Integer.parseInt((String) myProp.getOrDefault(MyProperties.TARATURA_CENTRO, "0"));
        //min = Integer.parseInt((String) myProp.getOrDefault(MyProperties.TARATURA_SINISTRA,"0"));
        //Da 0° a 180°, visualizza -90°,0,+90°
        int value = Utility.from10BitsToInt(buffer[6], buffer[7]) - centro;
        this.angolo = value;
        this.incrementaMarcia = Utility.fromByteMarciaInc(buffer[8]);
        this.decrementaMarcia = Utility.fromByteMarciaDec(buffer[8]);
    }

    public String toString() {
        return frame + "," + acceleratore + "," + frenoAnteriore + "," + frenoPosteriore
                + "," + angolo + "," + ((incrementaMarcia) ? 1 : 0) + "," + ((decrementaMarcia) ? 1 : 0);
    }

    public static ArrayList<DatiArduino> readCSV(String fileName) {

        //Student attributes index
        final int FRAME = 0;
        final int ACCELERATORE = 1;
        final int FRENO_ANTERIORE = 2;
        final int FRENO_POSTERIORE = 3;
        final int ANGOLO = 4;
        final int INCREMENTA_MARCIA = 5;
        final int DECREMENTA_MARCIA = 6;
        BufferedReader fileReader = null;
        ArrayList list = new ArrayList();
        try {

            String line = "";

            //Create the file reader
            fileReader = new BufferedReader(new FileReader(fileName));

            //Read the CSV file header to skip it
            fileReader.readLine();

            //Read the file line by line starting from the second line
            while ((line = fileReader.readLine()) != null) {
                //Get all tokens available in line
                String[] tokens = line.split(",");
                if (tokens.length > 0) {
                    //Create a new student object and fill his  data
                    DatiArduino dt = new DatiArduino();
                    dt.frame = Long.parseLong(tokens[FRAME]);
                    dt.acceleratore = Integer.parseInt(tokens[ACCELERATORE]);
                    dt.frenoAnteriore = Integer.parseInt(tokens[FRENO_ANTERIORE]);
                    dt.frenoPosteriore = Integer.parseInt(tokens[FRENO_POSTERIORE]);
                    dt.angolo = Integer.parseInt(tokens[ANGOLO]);
                    dt.incrementaMarcia = Integer.parseInt(tokens[INCREMENTA_MARCIA]) == 1;
                    dt.decrementaMarcia = Integer.parseInt(tokens[DECREMENTA_MARCIA]) == 1;

                    list.add(dt);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DatiArduino.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DatiArduino.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileReader.close();
            } catch (IOException ex) {
                Logger.getLogger(DatiArduino.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;
    }

    public static void createCSV(ArrayList<DatiArduino> ar, String fileName) {
        FileWriter fileWriter = null;

        try {
            //Write the CSV file header
            //fileWriter.append("Frame,acceleratore,FrenoAnteriore,FrenoPosteriore,Angolo,IncrementaMarcia,DecrementaMarcia");

            //Add a new line separator after the header
            fileWriter.append(System.getProperty("line.separator"));

            for (DatiArduino dt : ar) {
                fileWriter.append(dt.toString());
            }
        } catch (IOException ex) {
            Logger.getLogger(DatiArduino.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException ex) {
                Logger.getLogger(DatiArduino.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
