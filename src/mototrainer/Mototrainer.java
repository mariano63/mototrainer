package mototrainer;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.io.InputStream;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import video.TestVideoProcessing;

/**
 *
 * @author maria
 */
public class Mototrainer implements Observer{

    public static Font mioFont;
    
    DatiArduino ultimaLetturaArduino = new DatiArduino();

    static public Mototrainer motocicletta;
//    static public Wb wb;
    static public WbNew wbNew;
    public static AtomicBoolean isProcessingLock = new AtomicBoolean(true);
    
    @SuppressWarnings("empty-statement")
    public Mototrainer() {
        motocicletta=this;
        TestVideoProcessing.main("video.TestVideoProcessing");
        while(isProcessingLock.get());
        wbNew = new WbNew();
        
        
//        TestVideoProcessing.ErrorClass ec = 
//        TestVideoProcessing.iTestVideoProcessing.cErrorClass;
//        ec.setSkillLEvel(0);

    }

    private static void mettiSuIlMioFont(){
        try {
            //create the font to use. Specify the size!
            InputStream stream = ClassLoader.getSystemClassLoader().getResourceAsStream("res/steelfish_rg.otf");
            //InputStream stream = ClassLoader.getSystemClassLoader().getResourceAsStream("res/Need for Font.ttf");
            mioFont = Font.createFont(Font.TRUETYPE_FONT, stream).deriveFont(18f);
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            //register the font
            ge.registerFont(mioFont);
//            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("res/Need for Font.ttf")));
        } catch (FontFormatException | IOException ex) {
            Logger.getLogger(Mototrainer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        mettiSuIlMioFont();
        new Mototrainer();
//
//        wb = new Wb();
//        wb.setVisible(true);

        //COM
//        RxTx.listPorts();   //una lista delle porte
        
 
        //Sceglie la prima COMX disponibile
        
//        try {
//            (new TwoWaySerialComm()).connect("COM6");
//            System.out.println("COM1 connesso!");
//        } catch (Exception e) {
//            try {
//                (new TwoWaySerialComm()).connect("COM2");
//                System.out.println("COM2 connesso!");
//            } catch (Exception ex) {
//                try {
//                    (new TwoWaySerialComm()).connect("COM3");
//                    System.out.println("COM3 connesso!");
//                } catch (Exception ex1) {
//                    Logger.getLogger(Motocicletta.class.getName()).log(Level.SEVERE, null, ex1);
//                }
//            }
//        }
    }

    private String getFormattedSt(String s){
        return String.format("%3s", s);
    }
    
    @Override
    public void update(Observable o, Object arg) {
        
        
        if (arg instanceof byte[]) {
            //ultimi dati va in una struttura propria
            ultimaLetturaArduino.creaStruttura((byte[]) arg, MyProperties.PROPS);
//            System.out.println(ultimaLetturaArduino.toString());
  
            //Acceleratore
            wbNew.jLabelThrottleAcceleration.setText(getFormattedSt(ultimaLetturaArduino.acceleratore+""));
            wbNew.jLabelThrottleClose.setText(getFormattedSt(MyProperties.PROPS.getProperty(MyProperties.TARATURA_ACCELERATORE_MINIMO)));
            wbNew.jLabelThrottleOpen.setText(getFormattedSt(MyProperties.PROPS.getProperty(MyProperties.TARATURA_ACCELERATORE_MASSIMO)));
            //Inclinazione
            wbNew.jLabelThrottleInclination.setText(getFormattedSt(ultimaLetturaArduino.angolo-90+"°"));
            //Front Break
            wbNew.jLabelFrontBrake.setText(getFormattedSt(ultimaLetturaArduino.frenoAnteriore+""));
            wbNew.jLabelFrontBrakeAllClose.setText(getFormattedSt(MyProperties.PROPS.getProperty(MyProperties.TARATURA_ANTERIORE_MINIMO)));
            wbNew.jLabelFrontBrakeAllOpen.setText(getFormattedSt(MyProperties.PROPS.getProperty(MyProperties.TARATURA_ANTERIORE_MASSIMO)));
            //Rear Break
            wbNew.jLabelRearBrake.setText(getFormattedSt(ultimaLetturaArduino.frenoPosteriore+""));
            wbNew.jLabelRearBrakeAllClose.setText(getFormattedSt(MyProperties.PROPS.getProperty(MyProperties.TARATURA_POSTERIORE_MINIMO)));
            wbNew.jLabelRearBrakeAllOpen.setText(getFormattedSt(MyProperties.PROPS.getProperty(MyProperties.TARATURA_POSTERIORE_MASSIMO)));
            //repaint data
            wbNew.jPanelCalibration.repaint();
        
        }
    }

}
