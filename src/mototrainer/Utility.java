/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mototrainer;

import java.awt.Component;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.TableModel;

/**
 *
 * @author maria
 */
public class Utility {

    //0000_0xxx 0xxx_xxxx  2 byte (utilizzati solo 10 bit)
    static public int from10BitsToInt(int msb, int lsb) {
//        if ((msb & 0x01) != 0) {
//            lsb |= 0x80;
//        }
//        msb >>= 1;
//        return (msb & 0x07) * 256 + (lsb & 0x7f);
        int x;
        x = msb;
        x = x << 7;
        x = x | lsb;
        return x;
    }

    // ritorna 0 per incremento marcia, 1 decremento marcia
    //0000_00id            1 byte (utilizzati solo 2 bit)  incrementa marcia (0000_0010), decrementa marcia (0000_0001)
    static public boolean fromByteMarciaInc(byte b) {
        return ((b & (byte) 0x02) != 0);
    }

    static public boolean fromByteMarciaDec(byte b) {
        return ((b & (byte) 0x01) != 0);
    }

    static ArrayList<String> lookAndFeelList = new ArrayList();

    static ArrayList listInstalledLookandFeels() {
        lookAndFeelList.clear();
        UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
        for (UIManager.LookAndFeelInfo look : looks) {
            lookAndFeelList.add(look.getClassName());
            //System.out.println(look.getClassName());
        }
        System.out.println(lookAndFeelList.toString());
        return lookAndFeelList;
    }

    /**
     * Per il mio sistema windows 10 avrò:
     * javax.swing.plaf.metal.MetalLookAndFeel
     * javax.swing.plaf.nimbus.NimbusLookAndFeel
     * com.sun.java.swing.plaf.motif.MotifLookAndFeel
     * com.sun.java.swing.plaf.windows.WindowsLookAndFeel
     * com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel
     *
     * copia una di queste stringhe come parametro lf. *
     */
    static void setLF(String lf, Component frame) {
        //Qui carica la lista di LF
        listInstalledLookandFeels();
        if (lookAndFeelList.contains(lf)) {
            try {
                //ok trovato il L&F, lo setto
                UIManager.setLookAndFeel(lf);
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
            }
            SwingUtilities.updateComponentTreeUI(frame);
//            frame.pack();
        }
    }

    public static void copyFileUsingChannel(File source, File dest) throws IOException {

        if (dest.exists()) {
            int n = JOptionPane.showConfirmDialog(
                    Mototrainer.wbNew,
                    "Do you want overwrite the file?",
                    "File exist!",
                    JOptionPane.YES_NO_OPTION);
            if (n != JOptionPane.YES_OPTION) {
                return;
            }
        }

        dest.getParentFile().mkdirs();
        FileChannel sourceChannel = null;
        FileChannel destChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destChannel = new FileOutputStream(dest).getChannel();
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        } finally {
            if (sourceChannel != null) {
                sourceChannel.close();
            }
            if (destChannel != null) {
                destChannel.close();
            }
        }
    }

    public static void showOnScreen(int screen, JFrame frame) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gd = ge.getScreenDevices();
        if (screen > -1 && screen < gd.length) {
            frame.setLocation(gd[screen].getDefaultConfiguration().getBounds().x, frame.getY());
        } else if (gd.length > 0) {
            frame.setLocation(gd[0].getDefaultConfiguration().getBounds().x, frame.getY());
        } else {
            throw new RuntimeException("No Screens Found");
        }
    }

    public static void importFromCSV(JTable table,
            String pathToImport) throws IOException {

        List<String> list = Files.readAllLines(Paths.get(pathToImport), StandardCharsets.UTF_8);
        String[] a = list.toArray(new String[list.size()]);
        
        //la riga 0 contiene headers, quindi la skyppo
        for (int ndx=1; ndx< a.length; ndx++){
            String[] s = a[ndx].split(",");
            int row = ndx-1, col = 0;
            for (String val : s){
                table.setValueAt(val, row, col++);
            }
        }
        table.repaint();
    }

    public static boolean exportToCSV(JTable tableToExport,
            String pathToExportTo) {

        try {

            TableModel model = tableToExport.getModel();
            FileWriter csv = new FileWriter(new File(pathToExportTo));

            for (int i = 0; i < model.getColumnCount(); i++) {
                csv.write(model.getColumnName(i) + ",");
            }

            csv.write("\n");

            for (int i = 0; i < model.getRowCount(); i++) {
                for (int j = 0; j < model.getColumnCount(); j++) {
                    Object obj = model.getValueAt(i, j);
                    if (obj == null) {
                        obj = "";
                    }
                    csv.write(obj.toString() + ",");
                }
                csv.write("\n");
            }

            csv.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    static public void main(String[] args) {
        listInstalledLookandFeels();
    }

}
